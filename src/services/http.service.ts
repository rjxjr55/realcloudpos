import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response, URLSearchParams, ResponseOptions } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/Rx';
// import { setLoginSession } from '../app/apollo';
import { NetworkStatusService } from '../services/network-status.service';
import { Observer } from "rxjs/Rx";
import { API } from '../config';
import { FCM } from '@ionic-native/fcm';

@Injectable()
export class HttpService {
    public server_addr = API;

    public oauth: Oauth;
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers });
    public apikey = 'l7xx9c5343035508484aba02d77569cb40fc';

    constructor(private http: Http, public networkStatus: NetworkStatusService, private fcm: FCM) {
        this.oauth = new Oauth(this, this.http, this.networkStatus, fcm);
        // setLoginSession(this.oauth);
        this.setHeader();
    }

    private setHeader() {
        this.headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.oauth.getToken().access_token}` });
        this.options = new RequestOptions({ headers: this.headers });
    }

    public post(url: string, body = {}, params = {}): Observable<Response> {
        this.setHeader();
        var parameters = new URLSearchParams();
        parameters.set('apikey', this.apikey);
        for (var key in params) {
            parameters.set(key, params[key] + "");
        }
        this.options.search = parameters;
        if (this.networkStatus.bOnline) {
            return this.http.post(this.server_addr + url, body, this.options);
        }
        else
            return Observable.create((observer: Observer<null>) => {
                observer.error('network');
                observer.complete();
            });
    }

    public put(url: string, body = {}, params = {}): Observable<Response> {
        this.setHeader();
        var parameters = new URLSearchParams();
        parameters.set('apikey', this.apikey);
        for (var key in params) {
            parameters.set(key, params[key] + "");
        }
        this.options.search = parameters;
        if (this.networkStatus.bOnline) {
            return this.http.put(this.server_addr + url, body, this.options);
        }
        else
            return Observable.create((observer: Observer<null>) => {
                observer.error('network');
                observer.complete();
            });
    }

    public delete(url: string, params = {}): Observable<Response> {
        this.setHeader();
        var parameters = new URLSearchParams();
        parameters.set('apikey', this.apikey);
        for (var key in params) {
            parameters.set(key, params[key] + "");
        }
        this.options.search = parameters;
        if (this.networkStatus.bOnline) {
            return this.http.delete(this.server_addr + url, this.options);
        }
        else
            return Observable.create((observer: Observer<null>) => {
                observer.error('network');
                observer.complete();
            });

    }

    public get(url: String, params = {}): Observable<Response> {
        this.setHeader();
        var parameters = new URLSearchParams();
        parameters.set('apikey', this.apikey);
        for (var key in params) {
            parameters.set(key, params[key] + "");
        }
        this.options.search = parameters;
        if (this.networkStatus.bOnline) {
            return this.http.get(this.server_addr + url, this.options);
        }
        else
            return Observable.create((observer: Observer<null>) => {
                // observer.next(new Response( new ResponseOptions({ status: 499, statusText:'499',body:499 })));
                observer.error('network');
                observer.complete();
            });
    }
}

class Oauth {
    private token = {
        access_token: null,
        refresh_token: null,
        expires_in: null
    }

    constructor(private httpService: HttpService, private http: Http, public networkStatus: NetworkStatusService, private fcm: FCM) {
        this.setToken(localStorage.getItem('access_token'), localStorage.getItem('refresh_token'), localStorage.getItem('expires_in'));
        this.timer(this);
    }

    private timer(object) {
        var interval: number = 3000 * 1000; //ms
        var timer = Observable.timer(0, interval)
        timer.subscribe(
            function (x) {
                if (!object.getToken())
                    return;
                if (!object.getToken().expires_in)
                    return;
                if (object.getToken().expires_in <= Number(Date.now()) + interval) {
                    // console.log(object.getToken().expires_in);
                    // console.log(Number(Date.now()) + interval);
                    object.refresh();
                    console.log('토큰 재발급');
                }
                else {
                    // console.log(object.getToken().expires_in);
                    // console.log(Number(Date.now()) + interval);
                    console.log('수명 남음');
                }
            },
            function (err) {
                console.log('Error: ' + err);
            },
            function () {
                console.log('Completed');
            }
        );
    }

    private post(body = {}): Observable<Response> {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new RequestOptions({ headers: headers });
        var parameters = new URLSearchParams();
        parameters.set('apikey', this.httpService.apikey);
        options.search = parameters;

        if (this.networkStatus.bOnline) {
            return this.http.post(this.httpService.server_addr + '/auth/token', body, options);
        }
        else
            return Observable.create((observer: Observer<null>) => {
                observer.error('network');
                observer.complete();
            });
    }

    login(id: string, pwd: string) {
        var urlSearchParams = this.initSearchParams();
        urlSearchParams.append('grant_type', 'password');
        urlSearchParams.append('id', id);
        urlSearchParams.append('pwd', pwd);
        return this.post(urlSearchParams.toString())
    }

    logout() {
        let topic = localStorage.getItem('topic');
        if (topic)
            this.fcm.unsubscribeFromTopic(topic);
        this.setToken(null, null, null);
    }

    isLogin() {
        return this.getToken().access_token !== null;
    }

    refresh() {
        var urlSearchParams = this.initSearchParams();
        urlSearchParams.append('refresh_token', this.getToken().refresh_token);
        urlSearchParams.append('grant_type', 'refresh_token');
        this.post(urlSearchParams.toString())
        .subscribe(
            data => {
                this.setToken(data.json().access_token, data.json().refresh_token, data.json().expires_in);
            },
            error => {
                console.log('token refresh failed')
            }
        );
    }

    getToken() {
        return this.token;
    }

    setToken(nAccessToken, nRefreshToken, nExpiresIn) {
        localStorage.setItem('access_token', nAccessToken);
        localStorage.setItem('refresh_token', nRefreshToken);
        localStorage.setItem('expires_in', nExpiresIn);
        this.token.access_token = nAccessToken;
        this.token.refresh_token = nRefreshToken;
        this.token.expires_in = nExpiresIn;
    }

    initSearchParams() {
        var urlSearchParams = new URLSearchParams();
        urlSearchParams.append('client_id', 'dp');
        urlSearchParams.append('client_secret', 'dp');
        urlSearchParams.append('scope', 'all');
        return urlSearchParams;
    }
}