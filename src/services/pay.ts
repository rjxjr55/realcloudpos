import { Injectable } from "@angular/core";
import { HttpService } from './http.service';
import { Device } from "@ionic-native/device";
import { LoginSession } from "./login.session"

@Injectable()
export class Pay {

    private resource: string = '/pay';

    public pointPay: PointPay;
    public cardApproval: CardApproval;
    public end: End;

    constructor(private httpService: HttpService, 
                private loginSession: LoginSession,
                private device: Device) {
        this.pointPay = new PointPay(httpService);
        this.cardApproval = new CardApproval(httpService);
        this.end = new End(httpService);
    }

    // get(startDt, endDt, orderNo, pageNo = 1, pageSize = 20) {
    //     var params = {
    //         'startDt': startDt,
    //         'endDt': endDt,
    //         'orderNo': orderNo,
    //         'pageNo': pageNo,
    //         'pageSize': pageSize
    //     }
    //     return this.httpService.get(this.resource, params);
    // }
    post(orderNo, payTotal, cashAmt, cardAmt, mileageAmt, pointAmt, cashPrint, cardID, cardType, cardName, cardCompany, installmentMm, cardYYMM, approvalAmt, approvalNo, terminalStoreID, terminalID, tranID, barcode, userNo, curPoint) {
        if(this.loginSession.getInfo().posType == 1){
            var params_pp = {
                'posType': 1,
                // 'input': 
                'payTotal': payTotal,
                'cashAmt': cashAmt,
                'cardAmt': cardAmt,
                'pointAmt': pointAmt,
                'mileageAmt': mileageAmt,
                'cashPrint': cashPrint,
                'cardID': cardID,
                'cardType': cardType,
                'cardName': cardName,
                'cardCompany': cardCompany,
                'installmentMm': installmentMm,
                'cardYYMM': cardYYMM,
                'approvalAmt': approvalAmt,
                'approvalKey': approvalNo,
                'terminalID': terminalID,
                'terminalStoreID': terminalStoreID,
                'tranID': tranID,
                'barcode': barcode,
                'userNo': userNo,
                'curPoint': curPoint,
                'deviceId': this.device.uuid,            
                //인풋땡떙 다른거하나
            }
            return this.httpService.post(this.resource, params_pp);
        }
        else{
            var params = {
                'orderNo': orderNo,
                'payTotal': payTotal,
                'cashAmt': cashAmt,
                'cardAmt': cardAmt,
                'pointAmt': pointAmt,
                'mileageAmt': mileageAmt,
                'cashPrint': cashPrint,
                'cardID': cardID,
                'cardType': cardType,
                'cardName': cardName,
                'cardCompany': cardCompany,
                'installmentMm': installmentMm,
                'cardYYMM': cardYYMM,
                'approvalAmt': approvalAmt,
                'approvalKey': approvalNo,
                'terminalID': terminalID,
                'terminalStoreID': terminalStoreID,
                'tranID': tranID,
                'barcode': barcode,
                'userNo': userNo,
                'curPoint': curPoint,
                'deviceId': this.device.uuid,            
                //인풋땡떙 다른거하나
            }
            return this.httpService.post(this.resource, params);
        }
    }

    postCash(orderNo, payTotal, cashAmt, mileageAmt, pointAmt, cashPrint, barcode, userNo, curPoint) {
        var params = {
            'orderNo': orderNo,
            'payTotal': payTotal,
            'cashAmt': cashAmt,
            'pointAmt': pointAmt,
            'mileageAmt': mileageAmt,
            'cashPrint': cashPrint,
            'barcode': barcode,
            'userNo': userNo,
            'curPoint': curPoint,
            'deviceId': this.device.uuid,
            
        }
        return this.httpService.post(this.resource, params);
    }

    postCash_prepayment(input, detail, payTotal, cashAmt, mileageAmt, pointAmt, cashPrint, barcode, userNo, curPoint) {
        var params = {
            'posType': 1,
            'input': input,
            'details': detail,
            'payTotal': payTotal,
            'cashAmt': cashAmt,
            'pointAmt': pointAmt,
            'mileageAmt': mileageAmt,
            'cashPrint': cashPrint,
            'barcode': barcode,
            'userNo': userNo,
            'curPoint': curPoint,
            'deviceId': this.device.uuid,
            
        }
        return this.httpService.post(this.resource, params);
    }

    postCard(orderNo, payTotal,  cardAmt, mileageAmt, pointAmt,  cardID, cardType, cardName, cardCompany, installmentMm, cardYYMM, approvalAmt, approvalNo, terminalStoreID, terminalID, tranID, barcode, userNo, curPoint) {
        var params = {
            'orderNo': orderNo,
            'payTotal': payTotal,
            'cardAmt': cardAmt,
            'pointAmt': pointAmt,
            'mileageAmt': mileageAmt,
            'cardID': cardID,
            'cardType': cardType,
            'cardName': cardName,
            'cardCompany': cardCompany,
            'installmentMm': installmentMm,
            'cardYYMM': cardYYMM,
            'approvalAmt': approvalAmt,
            'approvalKey': approvalNo,
            'terminalID': terminalID,
            'terminalStoreID': terminalStoreID,
            'tranID': tranID,
            'barcode': barcode,
            'userNo': userNo,
            'curPoint': curPoint,
            'deviceId': this.device.uuid
        }
        return this.httpService.post(this.resource, params);
    }

    postCard_prepayment(input, detail, payTotal,  cardAmt, mileageAmt, pointAmt,  cardID, cardType, cardName, cardCompany, installmentMm, cardYYMM, approvalAmt, approvalNo, terminalStoreID, terminalID, tranID, barcode, userNo, curPoint) {
        var params = {
            'posType': 1,
            'input': input,
            'details': detail,
            'payTotal': payTotal,
            'cardAmt': cardAmt,
            'pointAmt': pointAmt,
            'mileageAmt': mileageAmt,
            'cardID': cardID,
            'cardType': cardType,
            'cardName': cardName,
            'cardCompany': cardCompany,
            'installmentMm': installmentMm,
            'cardYYMM': cardYYMM,
            'approvalAmt': approvalAmt,
            'approvalKey': approvalNo,
            'terminalID': terminalID,
            'terminalStoreID': terminalStoreID,
            'tranID': tranID,
            'barcode': barcode,
            'userNo': userNo,
            'curPoint': curPoint,
            'deviceId': this.device.uuid
        }
        return this.httpService.post(this.resource, params);
    }

    putCash(payNo, cashPrint) {
        var params = {
            'payNo': payNo,
            'cashPrint': cashPrint
        }
        return this.httpService.put(this.resource + '/cash', params);
    }



    postCredit(orderNo, creditTitle =null) {
        var params = {
            'orderNo': orderNo,
            'creditTitle':creditTitle
        }
        return this.httpService.put(this.resource + '/credit', params);
    }

    delete(payNo, orderNo) {
        var params = {
            'payNo': payNo,
            'orderNo': orderNo
        }
        return this.httpService.delete(this.resource, params);
    }
    getHistory(startDt, endDt,filter, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,            
            'filter': filter,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource + '/history', params);
    }
    getDetail(orderNo, pageNo = 1, pageSize = 20) {
        var params = {
            'orderNo': orderNo,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource + '/detail', params);
    }
}


export class PointPay {
    private resource: string = '/pay/pointpay';

    constructor(private httpService: HttpService) { }

    //스탬프  적립, 마일리지 적립때만 사용
    post(barcode,
        userNo,
        payAmt,
        stampCnt,
        mileageTranType,
        mileagePoint,
        saveRatio,
        //아래는 건드리지 않기! 안쓰는 애들 
        curPoint = 0,
        pointUse = 0,
        pointSave = 0,
        relNo = 0) {
        var params = {
            barcode: barcode,//사용자 전화번호
            userNo: userNo,//사용자 번호
            curPoint: curPoint,//userPoint
            payAmt: payAmt,//총결제금액
            pointUse: pointUse,//usePoint 사용할 포인트
            pointSave: pointSave, // 적립할 포인트
            stampCnt: stampCnt,//적립할 스탬프 
            relNo: relNo,//포인트, 마일리지 사용할때 나오는 payNo
            mileageTranType: mileageTranType,//마일리지 거래유형(1:적립,2:사용)
            mileagePoint: mileagePoint,//마일리지 포인트
            saveRatio: saveRatio,//마일리지 적립 비율 
        }
        return this.httpService.post(this.resource, params);
    }

}

export class CardApproval {
    private resource: string = '/pay/cardApproval';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, company = -1, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'company': company,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }
    getCompany() {
        return this.httpService.get(this.resource + '/company');
    }
    post(payNo, cardID, cardType, cardName, cardCompany, installmentMm, cardYYMM, approvalAmt, approvalNo, terminalStoreID, terminalID, TranID) {
        var params = {
            'payNo': payNo,
            'cardID': cardID,
            'cardType': cardType,
            'cardName': cardName,
            'cardCompany': cardCompany,
            'installmentMm': installmentMm,
            'cardYYMM': cardYYMM,
            'approvalAmt': approvalAmt,
            'approvalNo': approvalNo,
            'terminalID': terminalID,
            'terminalStoreID': terminalStoreID,
            'tranID': TranID
        }
        return this.httpService.post(this.resource, params);
    }


}

export class End {
    private resource: string = '/pay/end';

    constructor(private httpService: HttpService) { }

    get(orderNo) {
        var params = {
            'orderNo': orderNo
        }
        return this.httpService.get(this.resource, params);
    }


}