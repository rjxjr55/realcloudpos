import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Push {
    private resource: string = '/etc/push';

    constructor(private httpService: HttpService) {
    }

    post(userNo, sender, pushTitle, pushTxt, publicParm, sendType) {
        var params = {
            'userNo': userNo,
            'sender': sender,//topic storeNO
            'pushTitle': pushTitle,
            'pushTxt': pushTxt,
            'publicParm': publicParm,
            'sendType': sendType
        }
        return this.httpService.post(this.resource, params);
    }

    get(startDt, endDt, pageNo = 1, pageSize = 10, searchTxt = "") {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'pageNo': pageNo,
            'pageSize': pageSize,
            'searchTxt': searchTxt + ''
        }
        return this.httpService.get(this.resource, params);
    }
}


