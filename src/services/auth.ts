import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Auth {
    private resource: string = '/auth';

    constructor(private httpService: HttpService) { }

    getInfo() {
        return this.httpService.get(this.resource + '/info');
    }
}