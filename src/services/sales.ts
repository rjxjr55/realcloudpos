import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Sales {
    public order: Order;
    public start: Start;
    public end: End;

    constructor(private httpService: HttpService) {
        this.order = new Order(this.httpService);
        this.start = new Start(this.httpService);
        this.end = new End(this.httpService);

    }
}

export class Order {
    private resource: string = '/business/order';

    constructor(private httpService: HttpService) { }

    get(orderNo, orderStatus = 1) {

        var params = {
            orderStatus: orderStatus,
            orderNo: orderNo
        }
        return this.httpService.get(this.resource, params);
    }
    // getOrderOne(orderNo, orderStatus) {
    //     var params = {
    //         orderStatus: orderStatus,
    //         orderNo: orderNo
    //     }
    //     return this.httpService.get(this.resource + '/one', params);
    // }
    getDetail(orderNo, pageNo = 1, pageSize = 20) {
        var params = {
            orderNo: orderNo,
            'pageNo': pageNo,
            'pageSize': pageSize

        }
        return this.httpService.get(this.resource + '/detail', params)
    }
    postMaster(body) {
        return this.httpService.post(this.resource + '/master', body)
    }
    postDetailMulti(orderNo, orderList) {
        var body = {
            orderNo: orderNo,
            array: orderList
        }
        return this.httpService.post(this.resource + '/detail/multi', body)
    }
    put() {

    }
    delete(orderNo) {
        var params = {
            orderNo: orderNo
        }
        return this.httpService.delete(this.resource + '/master', params)
    }
}

export class Start {
    private resource: string = '/business/begin';

    constructor(private httpService: HttpService) { }

    post(openCash, salesDt) {
        var params = {
            openCash: openCash,
            salesDt: salesDt
        }
        return this.httpService.post(this.resource, params)
    }
}

export class End {
    private resource: string = '/business/end';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, endYn=0, pageNo = 1, pageSize = 20) {

        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'pageNo': pageNo,
            'pageSize': pageSize,
            'endYn':endYn
        }
        return this.httpService.get(this.resource, params);
    }
    getNo() {
        return this.httpService.get(this.resource + '/no');
    }

    post(salesDttm) {
        var params = {
            'salesDttm': salesDttm
        }
        return this.httpService.post(this.resource, params)
    }

    put(salesDttm, balanceCash) {
        var params = {
            'salesDttm': salesDttm,
            'balanceCash': balanceCash
        }
        return this.httpService.put(this.resource, params)
    }
    delete() {

        return this.httpService.delete(this.resource)
    }
}