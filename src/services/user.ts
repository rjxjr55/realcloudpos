import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class User {
    private resource: string = '/user';

    constructor(private httpService: HttpService) { }

    visit(name, tel) {
        return this.httpService.post('/store/user', {
            userName: name,
            tel: tel
        });
    }

    get(tel) {
        var params = {
            'tel': tel,
            'kiosk':false
        }
        return this.httpService.get(this.resource + '/search', params);
    }

    clear(userNo){
        return this.httpService.put('/store/user/visit', {userNo});
    }

    mileageLog(userNo, pageNo, pageSize) {
        return this.httpService.get(this.resource + '/mileage/log', { userNo: userNo, pageNo: pageNo, pageSize: pageSize });
    }

    stampLog(tel, pageNo, pageSize) {
        return this.httpService.get(this.resource + '/stamp', { tel: tel, pageNo: pageNo, pageSize: pageSize, startDt: '2000-01-01', endDt: '2999-12-31' });
    }
    stampUse(barcode, useCnt, useTxt) {
        var params = {
            'barcode': barcode, //폰번호
            'useCnt': useCnt,
            'useTxt': useTxt
        }
        return this.httpService.post(this.resource + '/stamp', params);
    }

    stampActive(userNo) {
        return this.httpService.get(this.resource + '/stampActive', { userNo: userNo });
    }

    client(search, page = 1, pageSize = 20) {
        var params = {
            search: search,
            pageNo: page,
            pageSize: pageSize
        }
        return this.httpService.get(this.resource + '/client', params);
    }
}

