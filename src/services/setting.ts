import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Setting {
    public floor: Floor;
    public table: Table;
    public menuCategory: MenuCategory;
    public menu: Menu;
    public subSetMenu: SubSetMenu;
    public setMenu: SetMenu;
    public attr: Attr;
    public menuOption: MenuOption;
    public customer: Customer;
    public dc: Dc;

    private resource: string = '/store';

    constructor(private httpService: HttpService) {
        this.floor = new Floor(httpService);
        this.table = new Table(httpService);
        this.menuCategory = new MenuCategory(httpService);
        this.menu = new Menu(httpService);
        this.subSetMenu = new SubSetMenu(httpService);
        this.setMenu = new SetMenu(httpService);
        this.attr = new Attr(httpService);
        this.menuOption = new MenuOption(httpService);
        this.customer = new Customer(httpService);
        this.dc = new Dc(httpService);
        
    }

    get() {
        return this.httpService.get(this.resource);
    }

    put(params) {
        var body = {
            'storeName': params.storeName,
            'storeID': params.storeID, 
            'posType': params.posType, 
            'storeDispName' : params.storeDispName, 
            'ceoName' : params.ceoName, 
            'tel' : params.tel, 
            'regDt' : params.regDt, 
            'approvalDt' : params.approvalDt,
            'stampSavingType' : params.stampSavingType, 
            'areaNo' : params.areaNo, 
            'areaName' : params.areaName, 
            'serviceStatus' : params.serviceStatus, 
            'serviceTimeText' : params.serviceTimeText, 
            'lat' : params.lat, 
            'lng' : params.lng, 
            'campaignText' : params.campaignText, 
            'storeText' : params.storeText,
            'directions' : params.directions, 
            'membershipText' : params.membershipText, 
            'hashTag' : params.hashTag, 
            'parking' : params.parking, 
            'addr1' : params.addr1, 
            'addr2' : params.addr2, 
            'zipcode' : params.zipcode, 
            'pointSavingRatio' : params.pointSavingRatio, 
            'pointSaveCash' : params.pointSaveCash, 
            'pointSaveCard' : params.pointSaveCard,
            'mileageSaveCash' : params.mileageSaveCash, 
            'mileageSaveCard' : params.mileageSaveCard, 
            'mileageStampOpt' : params.mileageStampOpt,
            'storeCategory' : params.storeCategory
        }

        return this.httpService.put(this.resource, body);
    }
}

export class Floor {
    private resource: string = '/store/floor';
    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }
    post(floorName, tableCNT) {
        var params = {
            'floorName': floorName,
            'tableCNT': tableCNT
        }
        return this.httpService.post(this.resource, params);
    }
    put(floorNo, floorName, dispOrder, tableCNT) {
        var body = {
            'floorNo': floorNo,
            'floorName': floorName,
            'dispOrder': dispOrder,
            'tableCNT': tableCNT
        }
        return this.httpService.put(this.resource, body);
    }
    delete(floorNo) {
        var params = {
            'floorNo': floorNo,
        }
        return this.httpService.delete(this.resource, params);
    }
}
export class Table {
    private resource: string = '/store/table';
    constructor(private httpService: HttpService) { }

    get(floorNo) {
        return this.httpService.get(this.resource + '/' + floorNo)
    }
    post(floorNo, tableName, seatCNT, dispShape) {
        var body = {
            floorNo: floorNo,
            tableName: tableName,
            seatCNT: seatCNT,
            dispShape: dispShape
        }
        return this.httpService.post(this.resource, body);
    }
    put(tableNo, tableName, seatCNT, dispOrder, dispShape) {
        var params = {
            'tableNo': tableNo,
            'tableName': tableName,
            'seatCNT': seatCNT,
            'dispOrder': dispOrder,
            dispShape: dispShape
        }
        return this.httpService.put(this.resource, params);
    }
    delete(floorNo, tableNo) {
        var params = {
            'floorNo': floorNo,
            'tableNo': tableNo
        }
        return this.httpService.delete(this.resource, params);
    }
}

export class MenuCategory {
    private resource: string = '/store/menuCategory';
    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }
    post(categoryName) {
        var params = {
            'categoryName': categoryName,
        }
        return this.httpService.post(this.resource, params);
    }
    put(categoryNo, categoryName, dispOrder, nlsCode) {
        var params = {
            'categoryNo': categoryNo,
            'categoryName': categoryName,
            'dispOrder': dispOrder,
            'nlsCode' : nlsCode
        }
        return this.httpService.put(this.resource, params);
    }
    delete(categoryNo) {
        var params = {
            'categoryNo': categoryNo
        }
        return this.httpService.delete(this.resource, params);
    }
}

export class Menu {
    private resource: string = '/store/menu';
    public bin: Bin;
    
    constructor(private httpService: HttpService) { 
        this.bin = new Bin(httpService);
    }

    get(categoryNo = "", menuType = -1, pageNo = 1, pageSize = 20) {
        var params = {
            'categoryNo': categoryNo + '',
            'menuType': menuType,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }
    getOne(menuNo: number) {
        return this.httpService.get(this.resource + '/' + menuNo);
    }
    getOrder(categoryNo = "", pageNo = 1, pageSize = 20) {
        var params = {
            'categoryNo': categoryNo + '',
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource + '/order', params);
    }
    post(body) {
        var params = body;
        return this.httpService.post(this.resource, params);
    }
    put(body) {
        var params = body;
        return this.httpService.put(this.resource, params);
    }
    delete(menuNo) {
        var params = {
            'menuNo': menuNo
        }
        return this.httpService.delete(this.resource, params);
    }
}

export class Bin {
    private resource: string = '/store/menu/bin';
    constructor(private httpService: HttpService) { }
    
    get() {
        return this.httpService.get(this.resource);
    }

    put(menuNo) {
        return this.httpService.put(this.resource, {menuNo: menuNo});
    }

    delete(menuNo) {
        return this.httpService.delete(this.resource, {menuNo: menuNo});
    }
}

export class EventList {
    private resource: string = '/store/eventList';
    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }
}
export class Event {
    private resource: string = '/store/event';
    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }
    post(eventNo, useYN, eventTitle, eventType, startDt, endDT, eventList) {
        var params = {
            'eventNo': eventNo,
            'useYN': useYN,
            'eventTitle': eventTitle,
            'eventType': eventType,
            'startDt': startDt,
            'endDT': endDT,
            'eventList': eventList
        }
        return this.httpService.post(this.resource, params);
    }
    put(eventNo, useYN, eventTitle, eventType, startDt, endDT, eventList) {
        var params = {
            'eventNo': eventNo,
            'useYN': useYN,
            'eventTitle': eventTitle,
            'eventType': eventType,
            'startDt': startDt,
            'endDT': endDT,
            'eventList': eventList
        }
        return this.httpService.put(this.resource, params);
    }
    // delete(eventNo) {
    //     var params = {
    //         'menueventNoNo': eventNo
    //     }
    //     return this.httpService.delete(this.resource, params);
    // }
}

export class SubSetMenu {
    private resource: string = '/store/setMenu/sub';
    constructor(private httpService: HttpService) { }

    get(menuNo, pageNo = 1, pageSize = 20) {
        var params = {
            'menuNo': menuNo,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }
    post(menuNo, subMenuNo, menuText, menuCNT) {
        var params = {
            'menuNo': menuNo,
            'subMenuNo': subMenuNo,
            'menuText': menuText,
            'menuCNT': menuCNT
        }
        return this.httpService.post(this.resource, params);
    }
    put(menuNo, subMenuNo, menuText, menuCNT) {
        var params = {
            'menuNo': menuNo,
            'subMenuNo': subMenuNo,
            'menuText': menuText,
            'menuCNT': menuCNT
        }
        return this.httpService.put(this.resource, params);
    }
    delete(menuNo, subMenuNo) {
        var params = {
            'menuNo': menuNo,
            'subMenuNo': subMenuNo
        }
        return this.httpService.delete(this.resource, params);
    }
}

export class SetMenu {
    private resource: string = '/store/setMenu';
    constructor(private httpService: HttpService) { }

    post(body) {
        var params = body;
        return this.httpService.post(this.resource, params);
    }
    delete(menuNo) {
        var params = {
            'menuNo': menuNo
        }
        return this.httpService.delete(this.resource, params);
    }
}

export class Attr {
    private resource: string = '/store/attr';
    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }
    post(detailCode) {
        var params = {
            detailCode: detailCode
        };
        return this.httpService.post(this.resource, params);
    }
}

export class MenuOption {
    private resource: string = '/store/menuOpt';
    constructor(private httpService: HttpService) { }

    get(menuNo) {
        var params = {
            menuNo: menuNo
        };
        return this.httpService.get(this.resource, params);
    }
    post(menuNo, optName, optAmt) {
        var params = {
            menuNo: menuNo,
            optName: optName,
            optAmt: optAmt
        };
        return this.httpService.post(this.resource, params);
    }
    put(menuNo, menuOptNo, optName, optAmt) {
        var params = {
            menuNo: menuNo,
            menuOptNo: menuOptNo,
            optName: optName,
            optAmt: optAmt
        };
        return this.httpService.put(this.resource, params);
    }
    delete(menuNo, menuOptNo) {
        var params = {
            menuNo: menuNo,
            menuOptNo: menuOptNo
        };
        return this.httpService.delete(this.resource, params);
    }
}

export class Customer {
    private resource: string = '/store/customer';
    constructor(private httpService: HttpService) { }

    get(pageNo = 1, pageSize = 7) {
        var params = {
            pageNo: pageNo,
            pageSize: pageSize
        };

        return this.httpService.get(this.resource, params);
    }
    post(params) {
        // var params = {
        //     customerNo: customerNo,
        //     customerName: customerName,
        //     customerID: customerID,
        //     supplyItem: supplyItem,
        //     ceoName: ceoName,
        //     ceoHP: ceoHP,
        //     tel: tel,
        //     fax: fax,

        // };
        return this.httpService.post(this.resource, params);
    }
    put(params) {
        // var params = {
        //     customerNo: customerNo,
        //     customerName: customerName,
        //     customerID: customerID,
        //     supplyItem: supplyItem,
        //     ceoName: ceoName,
        //     ceoHP: ceoHP,
        //     tel: tel,
        //     fax: fax,
        // };
        return this.httpService.put(this.resource, params);
    }
    delete(customerNo) {
        var params = {
            customerNo: customerNo,
        };
        return this.httpService.delete(this.resource, params);
    }
}

export class Dc {
    private resource: string = '/store/dc';
    constructor(private httpService: HttpService) { }
    
    get() {
        return this.httpService.get(this.resource);
    }

    post(item) {
        return this.httpService.post(this.resource, item);
    }

    delete(dcNo) {
        return this.httpService.delete(this.resource, {dcNo: dcNo});
    }
}
