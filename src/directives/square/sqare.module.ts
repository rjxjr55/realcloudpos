import { NgModule } from '@angular/core';
import { Square } from './square';

@NgModule({
    declarations: [Square],
    imports: [],
    exports: [Square],
    providers: [],
})
export class SquareModule {}