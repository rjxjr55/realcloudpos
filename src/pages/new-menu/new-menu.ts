import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ModalController, ToastController, Toast } from 'ionic-angular';
import { Setting } from '../../services/setting';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { PACKAGE_NAME } from '../../config';
import { Camera } from '@ionic-native/camera';
import { LoginSession } from "../../services/login.session";
import { ImageProvider } from "../../providers/image/image-provider";

@IonicPage()
@Component({
  selector: 'page-new-menu',
  templateUrl: 'new-menu.html',
})
export class NewMenuPage {
  categories: Array<any> = [];
  selectedCategoryIdx: number = 0;
  showDrop: boolean = false;
  floatEnabled: boolean = false;
  title: string = "신규메뉴 등록";
  locked: boolean = false;
  //taxAmt = 판매금액 / 11
  //stockCnt > 0 일때 stockSaleYN = 1
  newItem: any = {
    "menuNo": "", "menuCode": "", "menuName": "", "taxType": "1", "taxAmt": null, "unitPrice": null,
    "saleAmt": null, "categoryNo": "", "useYN": "0", "stampUseYN": "0", "soldOutYN": "0", "takeoutDCAmt": "",
    "regUserNo": "", "menuType": "1", "startDt": "2000-01-01", "endDt": "2099-12-31", "dispOrder": "", "menuBgColor": "",
    "stockSaleYN": "", "stockCNT": "", "originTxt": "원산지", "name": { "ko": null, "en": null, "cn":null }
  };
  menuImg: any = null;
  lang: string = this.tran.currentLang;
  isModalOpen = false;
  private toastInstance: Toast;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private setting: Setting, private events: Events, private modal: ModalController, public toastCtrl: ToastController, private loginSession: LoginSession, private imageProvider: ImageProvider, private tran:TranslateService, private camera: Camera) {
  }

  ionViewDidLoad() {
    this.categories = _.cloneDeep(this.navParams.get('categories'));
    this.selectedCategoryIdx = this.navParams.get('selectedCategoryIdx');
    this.categories.splice(0, 1);
    if(this.selectedCategoryIdx != 0) {
      this.selectedCategoryIdx -= 1;
    }

    if(this.navParams.get('item')) {
      this.newItem = this.navParams.get('item');
      this.newItem.soldOutYN == '1' ? this.locked = true : this.locked = false;
      if(this.newItem.thumbnailS)
        this.menuImg = this.newItem.thumbnailS;
      this.title = "메뉴 수정"
      console.log(JSON.stringify(this.newItem.name))
      console.log(this.lang)
      console.log(this.newItem.name[this.lang])
    }
  }

  modifyData() {
    // this.newItem.taxAmt = Math.floor(this.newItem.saleAmt / 11);

    if(this.newItem.stockCNT)
      this.newItem.stockSaleYN = "1";
    else
      this.newItem.stockSaleYN = "0";
    
    if(this.locked)
      this.newItem.soldOutYN = 1;
    else {
      this.newItem.soldOutYN = 0;
    }

    this.newItem.useYN = 1;

    this.newItem.categoryNo = this.categories[this.selectedCategoryIdx].id;
  }

  post() {
    this.modifyData();

    //나중에 바꿔야 함 0608
    if(!this.newItem.name[this.lang]) {
      this.toast('메뉴명을 입력해주세요.');
      return ;
    }
    if(this.newItem.unitPrice == null || this.newItem.unitPrice < 0) {
      this.toast('단가를 확인해주세요.');
      return ;
    }
    if(this.newItem.saleAmt == null || this.newItem.saleAmt < 0) {
      this.toast('판매금액을 입력해주세요.');
      return ;
    }
    if(!this.newItem.menuCode) {
      this.toast('메뉴코드를 입력해주세요.');
      return ;
    }
    let sendVal = this.newItem;
    sendVal.menuName = sendVal.name[this.lang];
    sendVal.nlsCode = this.lang;
    console.log(sendVal);
    this.setting.menu.post(sendVal)
    .subscribe(
      data => {
        this.toast('저장되었습니다.');
        this.close(true);
      },
      error => {
        console.log(error);
      }
    );
  }

  close(refresh = false) {
    if(refresh != false)
      this.events.publish('menu-list:refresh');
    this.viewCtrl.dismiss();
    this.isModalOpen = false;
  }

  recentMenu() {
    let modal = this.modal.create('DeletedMenuPage', {}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
    modal.present();
  }

  takePic(newImg: boolean = false) {
    let options = {
      destinationType: 0,
      sourceType: 1,
      encodingType: 0,
      quality: 50,
      allowEdit: false,
      saveToPhotoAlbum: false,
      correctOrientation: true  
    };

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
            if (data) {
              this.menuImg = data;
              console.log(this.menuImg);
            }
        });
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  getPic(newImg: boolean = false) {
     let options = {
      destinationType: 0,
      encodingType: 0,
      sourceType: 2,
      quality: 50
    };   

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
          if (data) {
            this.menuImg = data;
            // console.log(this.menuImg);
          }
        });
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  crop(imageData, callback) {
    let cropperModal = this.modal.create('ImageCropperPage', { imageData: imageData, aspectRatio: 249/178});
    cropperModal.onDidDismiss(data => {
      callback(data);
      this.floatEnabled = false;
    });
    cropperModal.present();
  }

  complete() {
    if(this.isModalOpen) return;

    this.isModalOpen = true;
    if(this.menuImg && !this.menuImg.startsWith("https://")) {
      var filename = this.loginSession.getInfo().storeNo + '_' + (new Date().getTime()) + Math.random() + '.jpg';
  
      this.imageProvider.s3.put(filename, this.menuImg).subscribe(
        res => {
          console.log(res);
        }
      );
  
      this.newItem.thumbnailS = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/s/store/" + filename;
      this.newItem.thumbnailM = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/m/store/" + filename;
      this.newItem.thumbnailL = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/l/store/" + filename;
    }
    this.post();
  }

  toast(text) {
    if(this.toastInstance) {
      return;
    }
      
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
  
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
  
    this.toastInstance.present();
  }
}
