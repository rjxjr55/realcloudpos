import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewMenuPage } from './new-menu';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    NewMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(NewMenuPage),
    AlertHeaderComponentModule,
  ],
  exports: [
    NewMenuPage
  ]
})
export class NewMenuPageModule {}
