import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, ModalController } from 'ionic-angular';
import { Apollo, QueryRef} from 'apollo-angular';
import gql from 'graphql-tag';
import { NetworkStatusService } from '../../services/network-status.service';
import { Setting } from '../../services/setting';

const Category = gql`
  query {
    getStore {
      id
      category {
        id
        name{
          ko
          en
          cn
          ja
        }
      }
    }
  }
`;

@IonicPage()
@Component({
  selector: 'page-order-dc',
  templateUrl: 'order-dc.html',
})

export class OrderDcPage {
  @ViewChild(Content) content: Content;
  categories: Array<any> = [];
  dcList: Array<any> = [];
  selectedDc: number = -1;
  bOnline: boolean = true;
  menuObs: QueryRef<any>;
  menuSub: any;
  editMode: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, private apollo: Apollo, public networkStatus: NetworkStatusService, private modal: ModalController, private setting: Setting) {
    networkStatus.networkStatus$.subscribe(res=>{
      this.bOnline = res;
    });
  }

  ionViewWillEnter() {
    this.getDcList();
    this.menuObs = this.apollo.watchQuery({
      query: Category,
      fetchPolicy: "cache-and-network"
    });
    this.menuSub = this.menuObs
    .valueChanges.subscribe(
      (res: any) =>{
        if (res.data) {
          var categories = [];
          categories = [...res.data.getStore.category];
          console.log(categories)
          categories.unshift({name: {ko: "전체"}});
          this.categories = categories;
          this.content.resize();
          this.events.publish('layout:menu-filter-height', this.content.getContentDimensions().contentTop);
        }
      }
    );
    this.events.subscribe('modal:new-dc', () => {
      this.newDc();
    });
    this.events.subscribe('dc:select-init', () => {
      this.selectedDc = -1;
    });
  }

  ionViewWillLeave() {
    console.log('dc ionViewWillLeave');

    this.menuSub.unsubscribe();
    this.events.unsubscribe('modal:new-dc');
    this.events.unsubscribe('dc:select-init');
    this.saveEdited();
  }

  getDcList() {
    this.setting.dc.get()
    .subscribe(
      data => {
        this.dcList = data.json().list;
      },
      error => {

      }
    );
  }
  deleteDc(item) {
    let modal = this.modal.create('ConfirmAlertPage', { title: '할인 삭제', label_small: '\''+item.dcName + '\'를 삭제하시겠습니까?', type: 1 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.res) {
        this.setting.dc.delete(item.dcNo)
        .subscribe(
          data => {
            console.log('dc 삭제 완료');
            this.getDcList();
          },
          error => {
          }
        );
      }
    });
    modal.present();
  }

  intoEditMode() {
    this.editMode = true;
    this.events.publish('layout:backdrop', this.editMode);
  }

  saveEdited() {
    this.editMode = false;
    this.events.publish('layout:backdrop', this.editMode);
  }

  applyDc(item, idx) {
    if(this.editMode)
      this.newDc(item);
    else {
      this.events.publish('dc:apply', item);
      this.selectedDc = idx;      
    }
  }

  newDc(item = null) {
    let modal = this.modal.create('NewDcPage', {item: item}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
    modal.onDidDismiss(
      data => {
        if(data) {
          this.setting.dc.post(data)
          .subscribe(
            data => {
              console.log('dc 수정 완료');
              this.getDcList();
            },
            error => {
            }
          );
        }
      }
    );
    modal.present();
  }
}
