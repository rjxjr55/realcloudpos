import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDcPage } from './order-dc';
import { SquareModule } from '../../directives/square/sqare.module';

@NgModule({
  declarations: [
    OrderDcPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderDcPage),
    SquareModule,
  ],
  exports: [
    OrderDcPage,
  ]
})
export class OrderDcPageModule {}
