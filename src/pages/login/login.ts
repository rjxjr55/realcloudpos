import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Events, Toast, Platform, IonicPage } from 'ionic-angular';
import { HttpService } from '../../services/http.service';
import { NativeStorage } from '@ionic-native/native-storage';
import { Sales } from '../../services/sales';
import { LoginSession } from '../../services/login.session';
import { Auth } from '../../services/auth';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';
import { CATIP } from '../../config';
// import { _ParseAST } from '@angular/compiler';
import { FCM } from '@ionic-native/fcm';

// declare const SMTCAT;
//declare const BIXOLON;
@IonicPage({
  priority: 'high'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  private id: string = '';
  private pwd: string = '';

  ip;

  idSaveCheck: boolean = false;

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private fcm: FCM, private http: HttpService, public toastCtrl: ToastController, private nativeStorage: NativeStorage, private events: Events, private sales: Sales, private auth: Auth, public loginSession: LoginSession, public platform: Platform, private cat: CatProtocolProvider) {
    let tmp = localStorage.getItem('CATIP');
    if (tmp && tmp != 'undefined')
      this.ip = tmp;
    else
      this.ip = CATIP;
  }

  ionViewDidLoad() {    
    this.events.publish('layout:header', '로 그 인', false, false, false);
    this.nativeStorage.getItem('loginId')
    .then(
      (data) => {
        this.id = data.id;
        this.idSaveCheck = true;

        // this.id = 'test';
        // this.pwd = '1234';
        
        // this.login();
      },
      error => {
      }
    );
    //this.cat.connect();

    //this.cat.connect();
    // if(this.platform.is('core') || this.platform.is('mobileweb')) {
    //   this.id = 'bongdal';
    //   this.pwd = '1234';
      
    //   this.login();
    // }
  }

  showSettings() {
    let ip = prompt('결제단말 IP', this.ip);
    if (ip) {
      this.ip = ip;
    }
  }

  login() {
    this.cat.connect(this.ip);
    // BIXOLON.connect(()=>{
    //   console.log('BIXOLON connected');
    // }, (e)=>{
    //   console.log(e);
    // });
    this.http.oauth.login(this.id, this.pwd)
    .subscribe(
      data => {
        this.http.oauth.setToken(data.json().access_token, data.json().refresh_token, data.json().expires_in);
        this.toast('로그인에 성공했습니다.');
        this.auth.getInfo()
        .map(data => data.json())
        .subscribe(
          data => {            
            this.loginSession.setInfo(data);
            localStorage.setItem('token', data.storeNo);
            this.fcm.subscribeToTopic(`order_${data.storeNo}`);
            this.sales.end.getNo()
              .subscribe(
                data => {                  
                  if (this.loginSession.getInfo().posType == 1) {
                    if (this.idSaveCheck)
                      this.saveId(this.goOrder);
                    else
                      this.clearSavedId(this.goOrder);
                  }
                  else {
                    if (this.idSaveCheck)
                      this.saveId(this.goOrder);
                    else
                      this.clearSavedId(this.goOrder);
                  }
                },
                err => {
                  if (err == "network")
                    console.log('인터넷 연결을 확인해 주세요.');
                  else {
                    if (this.idSaveCheck)
                      this.saveId(this.goBegin);
                    else
                      this.clearSavedId(this.goBegin);
                  }
                }
              );
          }
        );
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else{
          this.toast('아이디 혹은  비밀번호를 확인해 주세요.');
        console.log('login error: ' + JSON.stringify(err.json()));
        }
      }
    );
  }
  keyPress(event) {
    if (event.keyCode == 13) {
      this.login();
    }
  }

  saveId(func) {
    this.nativeStorage.setItem('loginId', { id: this.id })
    .then(
      () => {
        func(this.events);
      },
      error => {
        this.toast('아이디 저장에 실패했습니다');
        func(this.events);
      }
    );
  }

  clearSavedId(func) {
    this.nativeStorage.remove('loginId')
    .then(
      () => {
        func(this.events);
      },
      error => {
        func(this.events);
      }
    );
  }

  goBegin(events) {
    events.publish('nav:go', 4);
  }
  goOrder(events) {
    events.publish('nav:go', 0);
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

  register() {
    window.open('http://point.pohang.go.kr/apply');
  }

  email() {
    window.open('mailto:atmk98@handong.edu');
  }
}
