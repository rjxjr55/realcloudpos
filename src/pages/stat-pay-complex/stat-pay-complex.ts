import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, ToastController, Toast, Events } from 'ionic-angular';
import { Pay } from '../../services/pay';

@IonicPage()
@Component({
  selector: 'page-stat-pay-complex',
  templateUrl: 'stat-pay-complex.html',
})
export class StatPayComplexPage {
  title: string = "결제 내역";
  orderNo: any = 0;
  payInfo: any = { payNo: 0, payTotal: 0, payTypeName: '' };
  data: any[] = [{ checked: false, amt: 0, payType: "현금", payDttm: "2017-09-22 10:53:48", payNo: 947, payTotal: 25000 }];
  total: number = 0;
  pageNo: number = 1;
  cardNum: string = "";

  //카드 결제 정보 단말 연결되면 지우기 
  cardPayInfo = {
    'payNo': 0,
    'cardID': "0000-0000-0000-000",
    'cardType': "신용",
    'cardName': "삼성골드",
    'cardCompany': "삼성카드",
    'installmentMm': "0",
    'cardYYMM': "00-00",
    'approvalAmt': 0,
    'approvalNo': Math.ceil(Math.random() * 1000),
    'terminalID': "1234",
    'terminalStoreID': "181",
    'tranID': 123456
  }

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private pay: Pay, private viewCtrl: ViewController, private modalCtrl: ModalController, public toastCtrl: ToastController, private events: Events) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatPayComplexPage');
    this.orderNo = this.navParams.get('orderNo');
    this.getPayDetail();
  }

  getPayDetail(pageNo = 1) {
    this.pay.getDetail(this.orderNo, pageNo)
      .subscribe(
        data => {
          if (pageNo == 1) {
            this.data = [];
          }
          this.total = data.json().total;
          if (this.total - this.data.length > 0)
            for (var i = 0, k = 0; i < data.json().list.length; i++) {
              for (var j = 0; j < data.json().list[i].detail.length; j++ , k++) {
                this.data.push({
                  checked: false,
                  amt: data.json().list[i].detail[j].amt,
                  payType: data.json().list[i].detail[j].payType,
                  approvalNo: data.json().list[i].detail[j].approvalNo,
                  cardCompany: data.json().list[i].detail[j].cardCompany,
                  cardID: data.json().list[i].detail[j].cardID,
                  payDttm: data.json().list[i].payDttm,
                  payNo: data.json().list[i].payNo,
                  payTotal: data.json().list[i].payTotal
                });
              }
            }
          console.log(this.data);
        },
        err => {
          this.data = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
  }
  selectOrder(idx) {
    this.data.forEach((elemnet) => {
      elemnet.checked = false;
    });
    this.data[idx].checked = true;
    this.cardNum = this.data[idx].cardID;
    this.payInfo = this.data[idx];
    console.log(this.payInfo);
  }

  //버튼 클릭시
  //결제 취소 
  goPayDelete() {
    let modal;
    if (this.payInfo.payNo == 0)
      this.toast('결제를 선택해 주세요');
    else {
      if (this.payInfo.payTotal != this.payInfo.amt)
        modal = this.modalCtrl.create('ConfirmAlert2Page', { title: '복합 결제 취소', label_big: '포인트 또는 마일리지가 포함된 결제를 취소하시겠습니까?', label_payType: this.payInfo.payType, label_payAmt: this.payInfo.payTotal }, {
          cssClass: 'message-modal'
        });
      else {
        modal = this.modalCtrl.create('ConfirmAlert2Page', { title: '결제 취소', label_big: '결제를 취소하시겠습니까?', label_payType: this.payInfo.payType, label_payAmt: this.payInfo.payTotal }, {
          cssClass: 'message-modal'
        });
      }
      modal.onDidDismiss(data => {
        console.log(data);
        if (data.res == true) {
          if (this.payInfo.payType == '카드') {
            let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.payInfo.payTotal }, {
              cssClass: 'message-modal'
            });
            modal.onDidDismiss(data => {
              if (data.res == true)
                this.paydelete(true);
              //결제 단말기와 합쳐야할 부분 
              else
                this.toast('IC 삽입이 취소되었습니다. 결제 취소를 다시 시도해 주세요');
            });
            modal.present();
          } else {
            this.paydelete(true);
          }
        }
        else {//결제 취소를 취소 
          this.toast('취소되었습니다.');
        }
      });
      modal.present();
    }

  }


  goPayModify() {

    if (this.payInfo.payNo == undefined)
      this.toast('결제를 선택해 주세요');
    else if (this.payInfo.payType == '포인트' || this.payInfo.payType == '마일리지')
      this.toast('결제 수단 변경은 카드 결제와 현금 결제만 가능합니다.');
    else if (this.payInfo.payTotal != this.payInfo.amt)
      this.toast('마일리지와 포인트가 합쳐진 복합결제는 결제 취소 후 재결제해주세요..');
    else {
      let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: '결제 수단 변경', label_big: '기존 결제를 취소하시겠습니까?', label_payType: this.payInfo.payType, label_payAmt: this.payInfo.payTotal }, {
        cssClass: 'message-modal'
      });
      modal.onDidDismiss(data => {
        console.log(data);
        if (data.res == true) { // 결제 취소를 합니다
          if (this.payInfo.payType == '카드') {
            let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.payInfo.payTotal }, {
              cssClass: 'message-modal'
            });
            modal.onDidDismiss(data => {
              if (data.res == true)
                this.paydelete(false);
              //결제 단말기와 합쳐야할 부분 
              else
                this.toast('IC 삽입이 취소되었습니다. 결제 수단 변경을 다시 시도해 주세요');
            });
            modal.present();
          } else if (this.payInfo.payType == '현금') {
            this.paydelete(false);
          } else {
            this.toast('현금과 카드 결제일때만 결제 수단 변경이 가능합니다. 결제 수단을 확인해 주세요.');
          }
        }
        else {//결제 취소를 취소 
          this.toast('취소되었습니다.. ');
        }
      });
      modal.present();
    }
  }
  paydelete(val) {
    //val = true 결제 취소, val = false 결제 수단 변경 
    this.pay.delete(this.payInfo.payNo, this.orderNo)
      .subscribe(
        data => {
          if (val) {
            this.viewCtrl.dismiss();
            this.toast('결제 취소에 성공했습니다. 결제 화면으로 넘어갑니다.');
            this.events.publish('nav:goPay', this.orderNo, data.json()[0].remain);
          }
          else {
            this.toast('결제 수단 변경을 위해 기존 결제 취소에 성공했습니다.');
            this.rePay();
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        });
  }
  rePay() {
    let modal;
    if (this.payInfo.payType == '카드') {
      modal = this.modalCtrl.create('MessageAlertPage', { title: '결제 취소 후 현금 계산', message: '현금으로  ' + this.payInfo.payTotal + '원을 받아주세요', color: 'gray', closeAll: false }, { cssClass: "message-modal" });
      modal.onDidDismiss(() => {
        this.payCash();
      });
    }
    if (this.payInfo.payType == '현금') {
      modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.payInfo.payTotal }, {
        cssClass: 'message-modal'
      });
      modal.onDidDismiss(data => {
        if (data.res == true) {
          this.payCard();
        } else {
          this.toast('카드 결제 해주세요.');
          this.rePay();
        }
      });
    }
    modal.present();
  }

  //결제 하기 
  payCash() {
    //현금 쓰기
    // var payTax = Math.floor(this.dividePayTotal / this.taxRatio * 0.1);
    this.pay.postCash(
      this.orderNo, //주문 번호 
      this.payInfo.payTotal,//총 주문 금액 
      this.payInfo.payTotal,//cashAmt
      0,//mileageAmt
      0,//pointAmt
      null,//cashPrint
      null,//barcode
      null,//userNo
      null//curPoint
    )
      .subscribe(
        data => {
          this.toast('현금 결제 성공!');
          this.getPayDetail();
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('현금 결제 실패! 다시 시도해 주세요');
        }
      );
  }

  payCard() {
    //카드 쓰기 
    this.pay.postCard(
      this.orderNo, //주문 번호 
      this.payInfo.payTotal,//총 주문 금액 
      this.payInfo.payTotal,//cashAmt
      null,//mileageAmt
      null,//pointAmt
      this.cardPayInfo.cardID,
      this.cardPayInfo.cardType,
      this.cardPayInfo.cardName,
      this.cardPayInfo.cardCompany,
      1,
      this.cardPayInfo.cardYYMM,
      this.cardPayInfo.approvalAmt,
      this.cardPayInfo.approvalNo,
      this.cardPayInfo.terminalStoreID,
      this.cardPayInfo.terminalID,
      this.cardPayInfo.tranID,
      null,//barcode
      null,//userNo
      null//curPoint
    )
      .subscribe(
        data => {
          this.toast('카드 결제 성공!');
          this.getPayDetail();
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('카드 결제 실패! 다시 시도해 주세요.');
        }
      );
  }

  cashPrint() {
    console.log(this.payInfo);
    if (this.payInfo.payNo == undefined)
      this.toast('주문 혹은 결제를 선택해 주세요');
    else if (this.payInfo.payType != '현금')
      this.toast('현금으로 주문한 건만 현금영수증 발행이 가능합니다.');
    else if (this.payInfo.payTotal < 0)
      this.toast('취소 건은 현금영수증을 발급 할 수 없습니다.');
    else {
      let modal = this.modalCtrl.create('CashPrintPage', { data: this.payInfo, modified: true }, { cssClass: 'mileage-modal', enableBackdropDismiss: false });
      modal.onDidDismiss(data => {
        if (!data)
          this.toast('현금 영수증이 취소되었습니다.');
      });
      modal.present();
    }
  }

  receiptPrint() {
    if (this.payInfo.payNo == undefined) {
      this.toast('주문 혹은 결제를 선택해 주세요');
    }
    else {
      let modal = this.modalCtrl.create('MessageAlertPage', { title: '영수증 출력 완료', message: '감사합니다.', color: 'gray' }, { cssClass: "message-modal" });
      modal.present();
    }

  }

  preparing() {
    let modal = this.modalCtrl.create('MessageAlertPage', { title: '준비중', message: '준비중인 기능입니다.', color: 'red' }, { cssClass: "message-modal" });
    modal.present();
  }

  doInfinite() {
    this.pageNo++;
    this.getPayDetail(this.pageNo);
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }



  close() {
    this.viewCtrl.dismiss();
  }

}
