import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatPayComplexPage } from './stat-pay-complex';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { CheckboxModule } from '../../components/check-box/check-box.module';


@NgModule({
  declarations: [
    StatPayComplexPage,
  ],
  imports: [
    IonicPageModule.forChild(StatPayComplexPage),
    AlertHeaderComponentModule,
    CheckboxModule,
  ],
  exports: [
    StatPayComplexPage
  ]
})
export class StatPayComplexPageModule {}
