import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeginConfirmAlertPage } from './begin-confirm-alert';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    BeginConfirmAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(BeginConfirmAlertPage),
    AlertHeaderComponentModule
  ],
  exports: [
    BeginConfirmAlertPage
  ]
})
export class BeginConfirmAlertPageModule {}
