import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckMemberPage } from './check-member';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    CheckMemberPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckMemberPage),
    AlertHeaderComponentModule
  ],
  exports: [
    CheckMemberPage
  ]
})
export class CheckMemberPageModule {}
