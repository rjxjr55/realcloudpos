import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { User } from '../../services/user';
import { LoginSession } from '../../services/login.session';

@IonicPage()
@Component({
  selector: 'page-mileage-log',
  templateUrl: 'mileage-log.html',
})
export class MileageLogPage {
  log: Array<any> = [];
  userInfo: any = {};
  pageTitle: string = "";
  total:number = 0;
  pageSize = 10;
  pageNo = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private user: User, private loginSession: LoginSession, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.pageNo = 0;
    this.userInfo = this.navParams.get('userInfo');
    console.log( this.userInfo);
    this.doInfinite();
    }

  doInfinite(){
    this.pageNo++;
    if(this.loginSession.getInfo().mileageStampOpt == 1) {
      this.pageTitle = '마일리지 조회';
      this.getMileageLog();
    }
    else {
      this.pageTitle = '스탬프 조회';
      this.getStampLog();
    }
  }

  getMileageLog() {
    this.user.mileageLog(this.userInfo.userNo, this.pageNo, this.pageSize)
    .subscribe(
      data => {
        this.total = data.json().total;
        if (this.pageNo == 1) {
          this.log = [];
        }
        if (this.total - this.log.length > 0)
        for (var i = 0; i < data.json().list.length; i++) {
          this.log.push({
            mileage: data.json().list[i].mileage,
            storeName: data.json().list[i].storeName,
            storeNo: data.json().list[i].storeNo,
            tranDttm: data.json().list[i].tranDttm,
            tranType: data.json().list[i].tranType,
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  getStampLog() {
    this.user.stampLog(this.userInfo.tel, this.pageNo, this.pageSize)
    .subscribe(
      data => {
        this.total = data.json().total;
        if (this.pageNo == 1) {
          this.log = [];
        }
        if (this.total - this.log.length > 0)
        for (var i = 0; i < data.json().list.length; i++) {
          this.log.push({
            salesAmt: data.json().list[i].salesAmt,
            savingCnt: data.json().list[i].savingCnt,
            tel: data.json().list[i].tel,
            useCnt: data.json().list[i].useCnt,
            useTxt: data.json().list[i].useTxt,
            tranDttm: data.json().list[i].tranDttm,
          });
        }
      },
      error => {

      }
    );
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
