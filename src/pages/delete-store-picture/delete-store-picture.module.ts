import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteStorePicturePage } from './delete-store-picture';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    DeleteStorePicturePage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteStorePicturePage),
    AlertHeaderComponentModule
  ],
  exports: [
    DeleteStorePicturePage
  ]
})
export class DeleteStorePicturePageModule {}
