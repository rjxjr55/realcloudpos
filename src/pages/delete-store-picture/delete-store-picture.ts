import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-delete-store-picture',
  templateUrl: 'delete-store-picture.html',
})
export class DeleteStorePicturePage {
  pageTitle:string = "사진 삭제";
  which: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
  }

  yesOrNo(num) {
    this.which = num; // 0 => 삭제 안함 // 1 => 삭제함 //
    this.closeModal();
  }

  closeModal() {
    let data = { 'yesOrNo' : this.which };
    this.viewCtrl.dismiss(data);
  }

}
