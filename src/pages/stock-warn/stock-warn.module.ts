import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StockWarnPage } from './stock-warn';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { SquareModule } from '../../directives/square/sqare.module';

@NgModule({
  declarations: [
    StockWarnPage,
  ],
  imports: [
    IonicPageModule.forChild(StockWarnPage),
    AlertHeaderComponentModule,
    SquareModule
  ],
  exports: [
    StockWarnPage
  ]
})
export class StockWarnPageModule {}
