import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-stock-warn',
  templateUrl: 'stock-warn.html',
})
export class StockWarnPage {
  title: string = "재고 소진 임박";
  data: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.data = this.navParams.get('item');
    if(this.data.soldOutYN == 1 || (this.data.stockSaleYN == 1 && this.data.stockCNT == 0)) {
      this.title = "재고 소진 완료";
    }
    console.log(this.data);
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
