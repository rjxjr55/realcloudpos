import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaveMileageContainerPage } from './save-mileage-container';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SaveMileageContainerPage,
  ],
  imports: [
    IonicPageModule.forChild(SaveMileageContainerPage),
    AlertHeaderComponentModule,
    TranslateModule.forChild()
  ],
  exports: [
    SaveMileageContainerPage
  ]
})
export class SaveMileageContainerPageModule {}
