import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NgPipesModule } from 'ngx-pipes';
import { OrderTablePage } from './order-table';
import { SquareModule } from '../../directives/square/sqare.module';

@NgModule({
  declarations: [
    OrderTablePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderTablePage),
    SquareModule,
    NgPipesModule
  ],
  exports: [
    OrderTablePage
  ]
})
export class OrderTablePageModule {}
