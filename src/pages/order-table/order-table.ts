import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams, ModalController, Events, ToastController, Toast, IonicPage } from 'ionic-angular';
import { Apollo, QueryRef } from 'apollo-angular';
import { Device } from '@ionic-native/device';
import gql from 'graphql-tag';
import { NetworkStatusService } from '../../services/network-status.service';
import * as _ from "lodash";
import { Setting } from '../../services/setting';
import { LoginSession } from '../../services/login.session';

declare const $: any;

export const Order = gql`
  query { 
    getStore{
      id
      name
      floor {
        id
        name
        tableCNT
        dispOrder
      }
      table {
        id
        floorId
        name
        dispOrder
        dispShape
      }
      order {
        id
        tableId
        total
        dcTotal
        detail {
          id
          menuNo
          menuName
          diff
          itemCNT
          itemAmt
          stockCNT
          taxType
          dcAmt
          dcName
        }
      }
    }
  }
`;

const AddOrder = gql`
mutation AddOrder($input: OrderInput!, $details: [OrderDetailInput], $deviceId: String) {
  addOrder(input: $input, details: $details, deviceId: $deviceId) {
    id
    total
    detail {
      id
      menuNo
      menuName
      diff
      itemCNT
      itemAmt
      stockCNT
      taxType
      dcAmt
      dcName
    }
    error {
      key
      value
    }
  }
}
`;

const GetOrder = gql`
query GetOrder($id: Int!) {
  getOrder(id: $id) {
    id
    tableId
    total
    dcTotal
    detail {
      id
      menuNo
      menuName
      diff
      itemCNT
      itemAmt
      stockCNT
      taxType
      dcAmt
      dcName
    }
  }
}
`;

const DeleteOrder = gql`
mutation DeleteOrder($orderNo: Int!, $deviceId: String) {
  deleteOrder(orderNo: $orderNo, deviceId: $deviceId)
}
`;

@IonicPage()
@Component({
  selector: 'page-order-table',
  templateUrl: 'order-table.html'
})
export class OrderTablePage {
  store: any = null;
  storeObs: QueryRef<any>;
  storeSub: any;
  orderSubscription: any;
  selectedFloorIdx: number = 0;
  selectedTableIdx: number = 0;
  selectedTableId: number = 0;
  isLoading: boolean = true;
  bOnline: boolean = true;
  editMode: boolean = false;
  maxTable: number = 50;
  storename: any=this.loginSession.getInfo().storeName;

  private toastInstance: Toast;

  prepayment: Array<any> = []

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, private apollo: Apollo, public networkStatus: NetworkStatusService, private setting: Setting, private modal: ModalController, private toastCtrl: ToastController, private device: Device,private loginSession: LoginSession) {
    networkStatus.networkStatus$.subscribe(res => {
      this.bOnline = res;
    });
  }

  ionViewWillEnter() {
    this.events.subscribe('reloadTable', () => {      
      this.storeObs.refetch();
    });
    this.events.subscribe('table:add', () => {
      this.addTable();
    });

    this.events.subscribe('order:changed', (msg)=>{
      console.log('order:changed');
      this.apollo.query({
        query: GetOrder,
        fetchPolicy: 'network-only',
        variables: {
          id: msg.orderNo
        }
      })
      .subscribe((res: any)=>{
        if (!res.loading) {
          const id = res.data.getOrder.id;
          
          if (this.store.order.findIndex(_=>_.id == id) < 0) {
            let store = this.apollo.getClient();
            const data: any = store.readQuery({ query: Order });
            data.getStore.order.push(res.data.getOrder);
            store.writeQuery({ query: Order, data });
          }
        }
      });
    });
    this.events.subscribe('order:deleted', (msg)=>{
      let store = this.apollo.getClient();
      const data: any = store.readQuery({ query: Order });
      data.getStore.order.splice(data.getStore.order.findIndex(_=>_.id == msg.orderNo), 1);
      store.writeQuery({ query: Order, data });
    });

    //전영찬: selectedTableId를 order.ts로 넘기기 위해서 만듦
    this.events.subscribe('order:passPrepayment', selectedTableId => {
      this.showOrder(1, 1)
    })

    //전영찬: 이건 선불테이블들의 id를 넘ㄴ기기 위한 것
    this.events.subscribe('order:passPrepaymentId', (tableTags) => {
      console.log('order.passprepaymentid 콜됨')

      this.storeObs.valueChanges.subscribe(({ data, loading }) => {
        this.isLoading = loading;        
          
        if (!loading) {
          var i = 0;
          this.store = _.cloneDeep(data.getStore);

          // console.log(this.store.table)
  
          //전영찬: 흠 선불테이블의 각 tableId를 order.ts로 넘겨야 한다
          for(let i = 0; i < this.store.table.length; i++){
            if(this.store.table[i]['floorId'] == -1)
              this.prepayment.push({
                number: this.store.table[i]['id'],
                id: this.store.table[i]['id'],
                selected: false
              })
          }
          console.log(this.prepayment)
        }
        
        this.initDroppable();
      }, (err) => {
        console.log(err);
      })

      // for(let table of this.store.table){
      //   tableTags.push({
      //     number: table['id'] ,
      //     id: table['id'] ,
      //     selected: false
      //   })
      // }

      // for(let tag in tableTags){
      // }
    })

    this.storeObs = this.apollo.watchQuery({
      query: Order,
      fetchPolicy: "cache-and-network",
      variables:{
        nlsCode:'ko'
      }
    });

    // this.storeSub = this.bringData

    this.storeObs.valueChanges.subscribe(({ data, loading }) => {
      this.isLoading = loading;        
        
      if (!loading) {
        var i = 0;
        this.store = _.cloneDeep(data.getStore);

        // this.store.order.forEach(order => {
        //   var dcTotal: number = 0;
        //   order.detail.forEach(detail => {
        //     dcTotal += detail.itemCNT * detail.dcAmt;
        //   });
        //   order.dcTotal = !dcTotal ? 0 : dcTotal;

        //   this.store.table.forEach(table => {
        //     if(order.tableId == table.id)
        //       table.order = order;
        //   });
        // });

        //전영찬: 흠 선불테이블의 각 tableId를 order.ts로 넘겨야 한다
        for(let i = 0; i < this.store.table.length; i++){
          if(this.store.table[i]['floorId'] == -1)
            this.prepayment.push({
              number: this.store.table[i]['id'],
              id: this.store.table[i]['id'],
              selected: false
            })
        }
      }
      
      this.initDroppable();
    }, (err) => {
      console.log(err);
    });

    this.events.publish('layout:header', '영업', true, false, true);
    
  }

  ionViewWillUnload() {
    if (this.storeSub)
      this.storeSub.unsubscribe();
    if (this.orderSubscription)
       this.orderSubscription.unsubscribe();
    this.events.unsubscribe('reloadTable');
    this.events.unsubscribe('table:add');
    this.events.unsubscribe('order:changed');
    this.events.unsubscribe('order:deleted');
    this.saveEdited();
  }

  initDroppable() {
    // setTimeout(() => {
    //   $('.draggable').draggable({ revert: true, delay: 300 });
    //   $('.droppable').droppable({
    //     drop: (event, ui) => {
    //       var dragIndex = $(event.srcElement).attr("value");
    //       var dropIndex = $(event.target).attr("value");

    //       var dragItem = this.store.floor[this.selectedFloorIdx].table[dragIndex];
    //       var dropItem = this.store.floor[this.selectedFloorIdx].table[dropIndex];

    //       if(this.editMode) {
    //         var temp = dragItem.dispOrder;
    //         dragItem.dispOrder = dropItem.dispOrder;
    //         dropItem.dispOrder = temp;

    //         this.editTable(dragItem);
    //         if(dropItem.id)
    //           this.editTable(dropItem);
    //       }
    //       else {
    //         if(dropItem.id && (!dropItem.order || dropItem.order.total == 0))
    //           this.moveOrder(dragIndex, dropIndex);
    //         else if(dropItem.id){
    //           this.combineOrder(dragIndex, dropIndex);
    //         }
    //       }
    //     }
    //   });
    // }, 0);
  }

  editTable(item) {
    this.setting.table.put(item.id, "" + item.name, 0, item.dispOrder, item.dispShape)
    .subscribe(
      data => {
        console.log('테이블 수정 성공');
        this.storeObs.refetch();
      },
      error => {

      }
    );
  }

  // 주문 삭제
  cancelOrder(id) {
    this.apollo.mutate({
      mutation: DeleteOrder,
      variables: {
        orderNo: id
      }
    }).subscribe((res) => {
      console.log('삭제됨');
    });
  }

  getOrder(table) {
    return this.store.order.find(_=>_.tableId == table.id);
  }

  newOrder(tableItem) {
    console.log(tableItem);
    var vars = {
      input: {
        orderNo: null,
        orderName: tableItem.name,
        orderType: '1',
        floorNo: this.store.floor[this.selectedFloorIdx].id,
        waitingNo: -1,
        tables: "" + tableItem.id
      },
      details: tableItem.order ? this.getDetail(tableItem.order.detail) : [],
      deviceId: this.device.uuid
    };
    return this.apollo.mutate({
      mutation: AddOrder,
      variables: vars
    });
  }

  // 자리(주문) 이동
  moveOrder(dragIdx, dropIdx) {
    // TODO: dispOrd 만 변경하게 수정해야함!

    var dragItem = this.store.floor[this.selectedFloorIdx].table[dragIdx];
    var dropItem = this.store.floor[this.selectedFloorIdx].table[dropIdx];

    let modal = this.modal.create('ConfirmAlertPage', { title: '테이블 이동', 'label_big': dragItem.name + '번자리를 ' + dropItem.name + '번자리로 이동하시겠습니까?', 'label_small': '품목과 금액 모두 선택한 자리로 이동됩니다.', type: 0 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.res) {
        dropItem.order = dragItem.order;
        this.newOrder(dropItem)
          .subscribe(
          res => {
            console.log('이동 됨');
            // this.showOrder(dropIdx, this.store.floor[this.selectedFloorIdx].table[dropIdx].id);
            this.orderSelectInit();
          }
          );
        this.cancelOrder(dragItem.order.id);
      }
      else
        return;
    });
    modal.present();
  }

  // 자리(주문) 합석
  combineOrder(dragIdx, dropIdx) {
    // TODO: dispOrd 만 변경하게 수정해야함!

    var newIdArr: Array<any> = [];
    var newId: string = "";

    var dragItem = _.cloneDeep(this.store.floor[this.selectedFloorIdx].table[dragIdx]);
    var dropItem = _.cloneDeep(this.store.floor[this.selectedFloorIdx].table[dropIdx]);

    var newDetail = _.cloneDeep(dragItem.order.detail.concat(dropItem.order.detail));

    let modal = this.modal.create('ConfirmAlertPage', { title: '테이블 합석', 'label_big': dragItem.name + '번자리를 ' + dropItem.name + '번자리와 합석하시겠습니까?', 'label_small': '두 자리의 품목과 금액이 모두 합쳐집니다.', type: 0 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.res) {
        // create new table id
        this.store.floor[this.selectedFloorIdx].table.forEach(item => {
          if (item.order && ((item.order.id == dropItem.order.id) || (item.order.id == dragItem.order.id)))
            newIdArr.push(item.id);
        });
        newIdArr.forEach((item, index) => {
          newId += ("" + item + '|');
        });
        if (newId.length > 0)
          newId = newId.slice(0, -1);

        // create combined order array
        if (dropItem.order.id != dragItem.order.id) {
          newDetail.forEach((item, index) => {
            for (var i = index; i < newDetail.length - 1;) {
              i++;
              if (item.menuNo == newDetail[i].menuNo) {
                item.itemCNT += newDetail[i].itemCNT;
                newDetail.splice(i, 1);
              }
            }
          });

          // send request
          this.newOrder({ name: 'combinedOrder', id: newId, order: { detail: newDetail } })
            .subscribe(
            data => {
              console.log('합석');
              this.orderSelectInit();
              this.cancelOrder(dragItem.order.id);
              this.cancelOrder(dropItem.order.id);
            }
            );
        }
      }
      else
        return;
    });
    modal.present();


  }

  getDetail(detail) {
    detail.forEach(item => {
      delete item.id;
      delete item.stockCNT;
      delete item.__typename;
    });
    return detail;
  }

  showOrder(table, order) {
    console.log('showOrder called!')
    if (this.editMode) {
      console.log("in editmode")
      this.addTable(table);
      return;
    }
    if (this.selectedTableId == table.id) {
      console.log("selectedtableid == table.id")
      this.events.publish('nav:goToMenu');
    }
    this.selectedTableId = table.id;

    console.log(this.selectedTableId)
    this.events.publish('order:get', order, {
      table: table,
      floor: this.store.floor[this.selectedFloorIdx]
    });
  }

  changeFloor(idx) {
    if (this.editMode && (idx == this.selectedFloorIdx))
      this.editFloor(idx);
    else {
      this.selectedFloorIdx = idx;
      this.initDroppable();
    }
  }

  editFloor(idx) {
    let modal = this.modal.create('OneInputAlertPage', { title: '카테고리 수정', label: '카테고리명', 'input_str': this.store.floor[idx].name }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.val.length > 0) {
        this.setting.floor.put(this.store.floor[idx].id, data.val, this.store.floor[idx].dispOrder, this.store.floor[idx].tableCNT)
          .subscribe(
          data => {
            this.storeObs.refetch();
          }
          );
      }
    });
    modal.present();
  }

  removeFloor(idx) {
    let modal = this.modal.create('ConfirmAlertPage', { title: '층 삭제', label_small: this.store.floor[idx].name + '을 삭제하시겠습니까?', type: 1 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.res) {
        this.setting.floor.delete(this.store.floor[this.selectedFloorIdx].id)
        .subscribe(
          data => {
            this.storeObs.refetch().then(
              () => {
                if(this.store.floor.length > 0) {
                  this.changeFloor(0);
                }
              }
            )
            .catch(() => {});
          },
          error => {
            console.log('층 삭제 실패: ' + error);
            this.toast(error.json().message);
          }
        );
      }
    });
    modal.present();
  }

  addTable(item = null) {
    if(this.store.floor[this.selectedFloorIdx]) {
      let modal = this.modal.create('NewTablePage', { item: item, tables: this.store.table, floorNo: this.store.floor[this.selectedFloorIdx].id }, { cssClass: 'medium-modal', enableBackdropDismiss: false });
      modal.present();
    }
    else {
      this.toast('테이블을 추가할 수 있는 층이 없습니다. 층을 먼저 추가해주세요.')
    }
  }

  removeTable(item) {
    let modal = this.modal.create('ConfirmAlertPage', { title: '테이블 삭제', label_small: item.name + ' 번 테이블을 삭제하시겠습니까?', type: 1 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data.res) {
        this.setting.table.delete(this.store.floor[this.selectedFloorIdx].id, item.id)
          .subscribe(
            data => {
              this.storeObs.refetch();
            },
            error => {
            }
          );
      }
    });
    modal.present();
  }

  intoEditMode() {
    this.editMode = true;
    this.events.publish('layout:backdrop', this.editMode);
    this.initDroppable();
  }

  saveEdited() {
    this.editMode = false;
    this.events.publish('layout:backdrop', this.editMode);
    this.storeObs.refetch();
  }

  orderSelectInit() {
    this.events.publish('order:get', null);
    this.selectedTableId = -1;
    this.selectedTableIdx = -1;
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

  //전영찬: 겥스토어로 디스스토어에 데이터 불러오는 부분 함수로 빼버림
  // bringData = this.storeObs.valueChanges.subscribe(({ data, loading }) => {
  //   this.isLoading = loading;        
      
  //   if (!loading) {
  //     var i = 0;
  //     this.store = _.cloneDeep(data.getStore);

  //     // this.store.order.forEach(order => {
  //     //   var dcTotal: number = 0;
  //     //   order.detail.forEach(detail => {
  //     //     dcTotal += detail.itemCNT * detail.dcAmt;
  //     //   });
  //     //   order.dcTotal = !dcTotal ? 0 : dcTotal;

  //     //   this.store.table.forEach(table => {
  //     //     if(order.tableId == table.id)
  //     //       table.order = order;
  //     //   });
  //     // });

  //     //전영찬: 흠 선불테이블의 각 tableId를 order.ts로 넘겨야 한다
  //     for(let i = 0; i < this.store.table.length; i++){
  //       if(this.store.table[i]['floorId'] == -1)
  //         this.prepayment.push({
  //           number: this.store.table[i]['id'],
  //           id: this.store.table[i]['id'],
  //           selected: false
  //         })
  //     }
  //   }
    
  //   this.initDroppable();
  // }, (err) => {
  //   console.log(err);
  // });
}

