import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Toast, ToastController } from 'ionic-angular';
import _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-new-dc',
  templateUrl: 'new-dc.html',
})
export class NewDcPage {
  title: string = "할 인 추 가";
  item: any = { dcNo: null, dcName: "", dcType: 2, dcAmt: 0 };
  toastInstance: Toast = null;

  @ViewChild('dcAmt') dcAmt;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    if (this.navParams.get('item')) {
      this.title = "할 인 수 정";
      this.item = _.cloneDeep(this.navParams.get('item'));
    }
  }

  ionViewDidEnter() {
    this.dcAmt.focus();
  }

  close(tf = false) {
    if (tf && !this.item.dcName) {
      this.toast('할인명을 입력해주세요.');
      return;
    }
    if (tf && this.item.dcType == 2 && this.item.dcAmt > 100) {
      this.toast('퍼센트 할인의 할인율은 100%를 넘어갈 수 없습니다. ');
      return;
    }

    this.viewCtrl.dismiss(tf ? this.item : null);
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }
}
