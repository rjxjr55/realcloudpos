import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewDcPage } from './new-dc';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';

@NgModule({
  declarations: [
    NewDcPage,
  ],
  imports: [
    IonicPageModule.forChild(NewDcPage),
    AlertHeaderComponentModule,
    NumpadModule
  ],
  exports: [
    NewDcPage
  ]
})
export class NewDcPageModule {}
