import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreSettingPage } from './store-setting';
import { CheckboxModule } from "../../components/check-box/check-box.module";
import { NumpadModule } from "../../modules/Numpad/numpad.module";
import { MileageComponentModule } from "../../components/mileage/mileage.module";
import { TableSettingComponentModule } from '../../components/table-setting/table-setting.module';
import { TableAttrComponentModule } from '../../components/table-attr/table-attr.module';
import { EditorComponentModule } from '../../components/editor/editor.module';


@NgModule({
  declarations: [
    StoreSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(StoreSettingPage), 
    CheckboxModule,
    NumpadModule, 
    MileageComponentModule,
    TableSettingComponentModule,
    TableAttrComponentModule,
    EditorComponentModule,
  ],
  exports: [
    StoreSettingPage
  ]
})
export class StoreSettingPageModule { }
