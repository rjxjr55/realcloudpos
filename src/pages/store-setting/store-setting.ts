import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, Events, Toast, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Setting } from '../../services/setting';
import { Membership } from '../../services/membership';
import { ImageProvider } from '../../providers/image/image-provider'
import { LoginSession } from "../../services/login.session";
import { TableSettingComponent } from '../../components/table-setting/table-setting';

@IonicPage()
@Component({
  selector: 'page-store-setting',
  templateUrl: 'store-setting.html',
  providers: [ImageProvider]
})
export class StoreSettingPage {
  storeInfo: any = {
    "storeName": "", "storeID": "", "posType": "", "storeDispName": "", "ceoName": "", "tel": "", "regDt": "", "approvalDt": "",
    "stampSavingType": "", "areaNo": 0, "areaName": "", "serviceStatus": "", "serviceTimeText": "", "lat": "0", "lng": "0", "campaignText": "", "storeText": "",
    "directions": "", "membershipText": "", "hashTag": "", "parking": "", "addr1": "", "addr2": "", "zipcode": "00000", "pointSavingRatio": 0, "pointSaveCash": 3, "pointSaveCard": 1,
    "mileageSaveCash": 0, "mileageSaveCard": 0, "mileageStampOpt": 1, "storeCategory": ""
  };

  serviceStatus = ["", "준비중(심사대기)", " 영업중", " 임시휴무", "영업종료"];
  posType = ["", "선불 매장", "후불 매장"];
  selectedPosTypeIdx: number = 1;
  stampSavingType = ["미사용", "매출금액적립", "지정메뉴별적립"];
  mileageNStamp = ["", "마일리지", "스탬프"];
  sendButtun: boolean = false;

  whichsideTab: number = 1;
  whichCategory: any = 1;

  addPicture: boolean = false;
  image: Array<any> = [null, null, null, null, null, null, null, null, null]; // 저장이미지 최대개수 9개
  imageNum: number = 0;
  imageAllNum: number = 0;
  imageView = '';
  selectedPic: number;
  data: any[] = [];
  pageNo: number = 1;
  total: number = 0;
  noticeData = [{ noticeNo: '14', noticeTitle: '[공지] 업데이트 안내', regDttm: '2017-08-12', noticeTxt: '업데이트 안내입니다. ' }, { noticeNo: '13', noticeTitle: '[이벤트] 이벤트 당첨자 안내', regDttm: '2017-07-11', noticeTxt: '이벤트 당첨자 안내입니다. ' }]
  noticeData1 = [{ faqCategory: '주문결제', faqTitle: '주문결제를 어떻게 하나요 ?', faqTxt: '주문결제 안내' }, { faqCategory: '할인', faqTitle: '할인 하는 방법은?', faqTxt: '할인 안내' }];
  selectedMembership: any = 1;
  cashRatio: any = 0;
  cardRatio: any = 0;

  stampText: string = "";
  stampSavingAmt: number = 0;
  stampAttr: any = [];

  openMileageComponent: boolean = false;
  search: string = "";

  stamp1: any = 4; // 임시
  stamp2: any = 5; // 임시

  payType: any = { card: true, cash: true, hgu: true };

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private setting: Setting, private membership: Membership, private modalCtrl: ModalController, public toastCtrl: ToastController, public imageProvider: ImageProvider, public loginSession: LoginSession, public events: Events, public cdRef: ChangeDetectorRef, public loadingCtrl: LoadingController, private camera: Camera) {
  }

  ionViewDidLoad() {
    this.events.publish('layout:header', '설정', true, false, true);
    this.getStoreInfo();
    // this.getNotice();
  }

  // 사이드 탭 이동 
  clickSideTab(val) {
    this.whichsideTab = val;
    this.pageNo = 1;
    this.search = "";
    this.getInfo(val, 1);
  }

  getInfo(val, pageNo) {
    switch (val) {
      case 1:
        this.getStoreInfo();
        break;
      case 2:
        this.getStampSetting();
        break;
      case 3:
        this.getNotice(pageNo);
        break;
      case 5:
        this.getAttr(pageNo);
        break;

    }
  }

  OnPageChanged() {
    this.pageNo++;
    this.getInfo(this.whichsideTab, this.pageNo);
  }
  //매장 관리에 위에 카테고리 이동 
  clickCategory(val) {
    this.whichCategory = val;

    if (val == 5) this.getStoreImgList();
  }

  //첫번째 탭 매장 관리 
  getStoreInfo() {
    this.setting.get().subscribe(
      data => {
        this.storeInfo = data.json();
        this.toast("매장 정보를 가지고 왔습니다.")
        console.log(data.json());
        if (this.storeInfo.mileageStampOpt == 1) {
          this.selectedMembership = 1;
        }
        else {
          this.selectedMembership = 2;
        }
      },
      err => {
        if (err = "network") this.toast("인터넷 연결을 확인해 주세요.")
        else this.toast("오류입니다. 다시 시도해 주세요.")
      }
    );
  }

  modifyStoreInfo() {
    this.sendButtun = true;

    setTimeout(() => {

      this.setting.put(this.storeInfo).subscribe(
        data => {
          console.log(data.statusText);
          this.toast("저장되었습니다.");
          this.events.publish('fuction:loginSession');
          this.getStoreInfo();
          this.sendButtun = false;
        },
        err => {
          if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
          else this.toast("오류입니다. 다시 시도해 주세요.");
        }
      );
    }, 500);

  }

  getContentsData(event) {
    this.storeInfo.storeText = event;
    this.cdRef.detectChanges();
  }

  //매장 위치 설정 
  openMap() {
    let modal = this.modalCtrl.create('MapPage', { lat: this.storeInfo.lat, lng: this.storeInfo.lng }, { cssClass: 'mileage-modal' });
    modal.onDidDismiss(data => {
      this.storeInfo.lat = data.lat;
      this.storeInfo.lng = data.lng;
    });
    modal.present();
  }

  //매장 사진 설정 
  getStoreImgList() {
    // this.imageProvider.get(this.loginSession.getInfo().storeNo).subscribe(
    this.imageProvider.get().subscribe(
      res => {
        this.imageAllNum = this.imageNum = res.json().length;
        if (this.imageAllNum == 0) this.toast("이미지가 없습니다. 등록해 주세요.");
        //this.toast("이미지를 가져오는 중입니다."); --> 너무 말을 오래햇
        this.image = res.json();
        console.log(res.json());
      }
    );
  }

  modifyPic() {
    var data: Array<any> = [];
    var item: any;

    let loading = this.loadingCtrl.create({
      content: '이미지를 올리는 중입니다. 잠시만 기다려 주세요. '
    });

    loading.present();
    for (var i = 0; i < this.imageNum; i++) {
      if (this.image[i].url.substring(0, 4) != "http") { // 이미 올라간것들은 제외
        var filename = this.loginSession.getInfo().storeNo + '_' + (new Date().getTime()) + Math.random() + '.jpg';
        console.log("새 사진");


        this.imageProvider.s3.put(filename, this.image[i].url).subscribe(
          res => {
            console.log(res);
          }
        );

        item = {
          "dispOrder": i + 1,
          "url": "https://s3.ap-northeast-2.amazonaws.com/hgus3/origin/store/" + filename,
          "thumbnailL": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/l/store/" + filename,
          "thumbnailM": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/m/store/" + filename,
          "thumbnailS": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/s/store/" + filename
        };
        console.log(item);

      } else {
        item = this.image[i];
        item.dispOrder = i + 1;
        console.log("이미 올림");

      }
      data.push(item);
    }

    this.imageProvider.deleteAll().subscribe(
      res => {
        // this.toast('이미지를 올리는 중입니다. 잠시만 기다려 주세요.', 500);
        var sync = (index) => {
          if (index < this.imageNum) {
            this.imageProvider.post(data[index].url, data[index].thumbnailS, data[index].thumbnailM, data[index].thumbnailL, index + 1).subscribe(
              res => {
                console.log(data);

                sync(index + 1);
                if (index + 1 == this.imageNum) {
                  loading.dismiss();
                  this.events.publish('fuction:loginSession');
                  this.toast("이미지 업로드 성공!");
                  // setTimeout(() => {
                  //   this.toast("이미지 업로드 성공!");
                  // }, 2000);
                }
              }
            );
          }
        }
        sync(0);
      }
    );
  }

  selectPic(num) {
    if (num < this.imageNum) {
      this.selectedPic = num;
      this.imageView = this.image[num].url;
    }
  } // 오른쪽 화면으로 크게보기

  takepic(newImg: boolean = false) {
    this.addPicture = false;

    let options = {
      destinationType: 0,
      sourceType: 1,
      encodingType: 0,
      quality: 50,
      allowEdit: false,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
          if (data) {
            this.image[this.imageNum] = { url: data };
            this.imageNum++;
          }
        });
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  getpic(newImg: boolean = false) {
    this.addPicture = false;

    let options = {
      destinationType: 0,
      encodingType: 0,
      sourceType: 2,
      quality: 50
    };

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
          if (data) {
            this.image[this.imageNum] = { url: data };
            this.imageNum++;
          }
        });
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });

  }

  crop(imageData, callback) {
    let cropperModal = this.modalCtrl.create('ImageCropperPage', { imageData: imageData, aspectRatio: 249 / 178 });
    cropperModal.onDidDismiss(data => {
      callback(data);
    });
    cropperModal.present();
  }


  deletePicture() {
    if (this.selectedPic == null) {
      this.toast("삭제할 사진을 선택해 주세요.");
    }
    else {
      let modal = this.modalCtrl.create('DeleteStorePicturePage', {}, { cssClass: 'message-modal' });
      modal.onDidDismiss(data => {
        console.log(data);

        if (data.yesOrNo == 1) {
          this.image.splice(this.selectedPic, 1);
          this.image[7] = null;
          this.imageNum--;
          this.selectedPic = null;//
          this.imageView = '';//
          this.toast("사진을 삭제하였습니다.");
        }
      });
      modal.present();
    }
  }
  getStampSetting() {
    this.membership.stampSet.get().subscribe(
      data => {
        this.stampText = data.json().stampText;
        this.stampSavingAmt = data.json().stampSavingAmt;
        this.stampAttr = data.json().attr;
      },
      err => {
        this.stampText = "'여기'를 클릭해서 스탬프 설정을 추가해주세요! 추가하셔야 스탬프 발급/사용이 가능합니다.";
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else if (err.json().message == "TypeError: Cannot read property 'stampSavingAmt' of undefined") this.toast("'여기'를 클릭해서 스탬프 설정을 추가해주세요! 추가하셔야 스탬프 발급/사용이 가능합니다.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }
  //고객혜택설정
  whichMembership(val) {
    this.selectedMembership = val;

    if (this.selectedMembership == 1) { // cash
      this.storeInfo.mileageStampOpt = 1;
    }
    else if (this.selectedMembership == 2) { // card
      this.storeInfo.mileageStampOpt = 2;
    }
  }

  cash_pm(val) {
    if (val == '+')
      this.storeInfo.mileageSaveCash++;
    else if (val == '-' && this.storeInfo.mileageSaveCash > 0)
      this.storeInfo.mileageSaveCash--;
  }

  card_pm(val) {
    if (val == '+')
      this.storeInfo.mileageSaveCard++;
    else if (val == '-' && this.storeInfo.mileageSaveCard > 0)
      this.storeInfo.mileageSaveCard--;
  }

  stamp_pm(index, val) {
    if (index == 1) {
      if (val == '+') this.stamp1++;
      else if (val == '-') this.stamp1--;
    }
    else if (index == 2) {
      if (val == '+') this.stamp2++;
      else if (val == '-') this.stamp2--;
    }
  }

  letsTest() {
    this.modifyPic();
  }

  calDate(date) {
    var temp: any = new Date(date);
    var result: String;

    result = temp.getFullYear() + '-' + (Number(temp.getMonth()) + 1) + '-' + temp.getDate();
    return result;
  }

  goStampSet() {
    let modal = this.modalCtrl.create('StampSetPage', { type: 'set', stampText: this.stampText, stampSavingAmt: this.stampSavingAmt }, { cssClass: "medium-modal" });
    modal.onDidDismiss(() => {
      this.getStampSetting();
    });
    modal.present();
  }

  goStampAttr(item = null) {
    let modal = this.modalCtrl.create('StampSetPage', { type: 'attr', attrItem: item }, { cssClass: "medium-modal" });
    modal.onDidDismiss(() => {
      this.getStampSetting();
    });
    modal.present();
  }

  deleteStampAttr(item) {
    this.membership.stampAttr.delete(item.stampUnit).subscribe(
      data => {
        this.toast("성공적으로 삭제했습니다.");
        this.getStampSetting();
      },
      err => {
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }

  getNotice(pageNo = 1) {
    var filter = -1;
    this.membership.notice.get(this.search, filter, pageNo).subscribe(
      data => {
        this.total = data.json().total;

        if (pageNo == 1) {
          this.data = [];
          if (data.json().list.length == 0)
            this.toast('공지사항이 없습니다.');
          else
            this.toast('공지사항을 가지고 왔습니다.');
        }
        if (this.total - this.data.length > 0) {
          for (var i = 0; i < data.json().list.length; i++) {
            this.data.push({
              noticeNo: data.json().list[i].noticeNo,
              noticeText: data.json().list[i].noticeText,
              noticeTitle: data.json().list[i].noticeTitle,
              regDttm: data.json().list[i].regDttm
            });
          }
        }
      },
      err => {
        this.data = [];
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }

  getAttr(pageNo = 1) {
    this.setting.attr.get().subscribe(
      data => {

        this.data = [];
        if (data.json().length == 0)
          this.toast('매장 속성이 없습니다.');
        else
          this.toast('매장 속성을 가지고 왔습니다.');

        for (var i = 0; i < data.json().length; i++) {
          this.data.push({
            codeName: data.json()[i].codeName,
            detailCode: data.json()[i].detailCode,
            useYN: data.json()[i].useYN == "1" ? true : false
          });
        }
        console.log(this.data);
      },
      err => {
        this.data = [];
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }
  toggle(event) {
    this.setting.attr.post(event.detailCode).subscribe(
      data => {
      },
      err => {
        this.data = [];
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }


  toast(text, duration = 2000) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: duration,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

}
