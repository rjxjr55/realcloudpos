import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatMenuOrderPage } from './stat-menu-order';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    StatMenuOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(StatMenuOrderPage),
    AlertHeaderComponentModule,
  ],
  exports: [
    StatMenuOrderPage
  ]
})
export class StatMenuOrderPageModule {}
