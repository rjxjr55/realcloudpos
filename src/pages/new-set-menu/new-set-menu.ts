import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ModalController, ToastController, Toast } from 'ionic-angular';
import { Setting } from '../../services/setting';
import * as _ from 'lodash';
import { Camera } from '@ionic-native/camera';
import { ImageProvider } from "../../providers/image/image-provider";
import { ImageCropperPage } from "../image-cropper/image-cropper";
import { LoginSession } from "../../services/login.session";
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-new-set-menu',
  templateUrl: 'new-set-menu.html',
})
export class NewSetMenuPage {
  allMenu: Array<any> = [];
  categories: Array<any> = [];
  selectedCategoryIdx: number = 0;
  showDrop: boolean = false;
  showMenuSummury: boolean = false;
  floatEnabled: boolean = false;
  title: string = "세트메뉴 등록";
  locked: boolean = false;
  showMenuDrop: boolean = false;
  //taxAmt = 판매금액 / 11
  //stockCnt > 0 일때 stockSaleYN = 1
  newItem: any = {
    "menuNo": "", "menuCode": "", "menuName": "", "taxType": "1", "taxAmt": null, "unitPrice": null,
    "saleAmt": null, "categoryNo": "", "useYN": "0", "stampUseYN": "0", "soldOutYN": "0", "takeoutDCAmt": "",
    "regUserNo": "", "menuType": "2", "startDt": "2000-01-01", "endDt": "2099-12-31", "dispOrder": "", "menuBgColor": "",
    "stockSaleYN": "", "stockCNT": "", "originTxt": "원산지", "name": { "ko": null, "en": null, "cn":null }, "subList": []
  };
  itemSelected: Array<any> = [];
  totalPrice: number = 0;

  setMenuImg: any = null;
  lang: string = this.tran.currentLang;
  isModalOpen = false;
  private toastInstance: Toast;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private setting: Setting, private events: Events, private modal: ModalController, public toastCtrl: ToastController, private loginSession: LoginSession, private imageProvider: ImageProvider, private tran: TranslateService, private camera: Camera) {
  }

  initialize() {
    this.categories = _.cloneDeep(this.navParams.get('categories'));
    this.categories.shift();
    this.selectedCategoryIdx = this.navParams.get('selectedCategoryIdx');
    if(this.selectedCategoryIdx != 0) {
      this.selectedCategoryIdx -= 1;
    }
    this.allMenu = _.flatten(this.categories)[0].menu;
    
    this.allMenu = this.allMenu.filter(
      (item) => {
        return item.menuType == 1;
      }
    );
    this.itemSelected = this.getSelected();
    this.showMenuSummury = false;
  }

  ionViewDidLoad() {
    this.initialize();
    // edit set menu
    if(this.navParams.get('item')) {
      // data to variable
      this.newItem = _.cloneDeep(this.navParams.get('item'));
      // subList to itemSelected
      this.newItem.subList.forEach(item => {
        this.allMenu.forEach(element => {
          if(item.subMenuNo == element.id) {
            element.checked = true;
            element.cnt = item.menuCNT;
          }
        });
      });
      this.checkedChange();
      this.showMenuSummury = true;
      // check menu lock
      this.newItem.soldOutYN == '1' ? this.locked = true : this.locked = false;
      if(this.newItem.thumbnailS)
        this.setMenuImg = this.newItem.thumbnailS;
      this.title = "세트메뉴 수정";
      console.log(JSON.stringify(this.newItem));
    }
  }

  modifyData() {
    // this.newItem.taxAmt = Math.floor(this.newItem.saleAmt / 11);

    if(this.newItem.stockCNT)
      this.newItem.stockSaleYN = "1";
    else
      this.newItem.stockSaleYN = "0";
      
    if(this.locked)
      this.newItem.soldOutYN = 1;
    else {
      this.newItem.soldOutYN = 0;
    }

    this.newItem.useYN = 1;

    this.newItem.categoryNo = this.categories[this.selectedCategoryIdx].id;

    this.newItem.subList = [];
    this.itemSelected.forEach(item => {
      this.newItem.subList.push({subMenuNo: item.id, menuText: item.menuName, menuCNT: item.cnt});
    });
  }

  post() {
    this.modifyData();
    let sendVal = this.newItem;
    sendVal.menuName = sendVal.name[this.lang];
    sendVal.nlsCode = this.lang;
    this.setting.menu.post(sendVal)
    .subscribe(
      data => {
        this.close(true);
      },
      error => {
        console.log(JSON.stringify(error));
      }
    );
  }

  close(refresh = false) {
    if(refresh != false)
      this.events.publish('menu-list:refresh');
    this.viewCtrl.dismiss();
    this.isModalOpen = false;
  }

  pm(val, item) {
    if(item.cnt == undefined)
      item.cnt = 1;
    
    if(val == '+')
      item.cnt++;
    else if(val == '-' && item.cnt > 1)
      item.cnt--;

    this.checkedChange();
  }

  getSelected() {
    return this.allMenu.filter(
      (item) => {
        return item.checked;
      }
    );
  }

  checkedChange(item = null) {
    if(item && item.cnt == undefined)
      item.cnt = 1;
    
    this.totalPrice = 0;
    this.itemSelected = this.getSelected();
    for(var i = 0; i < this.itemSelected.length; i++) {
      this.totalPrice += this.itemSelected[i].saleAmt * this.itemSelected[i].cnt;
    }
  }

  selectMenu(con) {
    console.log(con);
    if(con == 0 || this.itemSelected.length < 1) {  //cancel
      this.showDrop = false;
      this.showMenuDrop = false;
      this.showMenuSummury = false;
    }
    else { //ok
      this.showDrop = false;
      this.showMenuDrop = false;
      this.showMenuSummury = true;
    }
  }

  recentMenu() {
    let modal = this.modal.create('DeletedMenuPage', {}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
    modal.present();
  }

  takePic(newImg: boolean = false) {
    let options = {
      destinationType: 0,
      sourceType: 1,
      encodingType: 0,
      quality: 50,
      allowEdit: false,
      saveToPhotoAlbum: false,
      correctOrientation: true  
    };

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
            if (data) {
              this.setMenuImg = data;
              // console.log(this.setMenuImg);
            }
        });        
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  getPic(newImg: boolean = false) {
     let options = {
      destinationType: 0,
      encodingType: 0,
      sourceType: 2,
      quality: 50
    };   

    this.camera.getPicture(options).then(
      (data) => {
        var imgdata = "data:image/jpeg;base64," + data;
        this.crop(imgdata, (data) => {
          if (data) {
            this.setMenuImg = data;
            // console.log(this.setMenuImg);
          }
        });
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  crop(imageData, callback) {
    let cropperModal = this.modal.create(ImageCropperPage, { imageData: imageData, aspectRatio: 249/178});
    cropperModal.onDidDismiss(data => {
      callback(data);
      this.floatEnabled = false;
    });
    cropperModal.present();
  }

  complete() {
    if(this.isModalOpen) return;

    this.isModalOpen = true;
    if(this.newItem.menuNo && this.setMenuImg && !this.setMenuImg.startsWith("https://")) {
      var data: Array<any> = [];
      var item: any;

      var filename = this.loginSession.getInfo().storeNo + '_' + (new Date().getTime()) + Math.random() + '.jpg';

      this.imageProvider.s3.put(filename, this.setMenuImg).subscribe(
        res => {
          console.log('===============')
          console.log(JSON.stringify(res));
          console.log('===============')
        }
      );

      this.newItem.thumbnailS = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/s/store/" + filename;
      this.newItem.thumbnailM = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/m/store/" + filename;
      this.newItem.thumbnailL = "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/l/store/" + filename;
    }

    this.post();
  }

  toast(text) {
    if(this.toastInstance) {
      return;
    }
      
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
  
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
  
    this.toastInstance.present();
  }
}
