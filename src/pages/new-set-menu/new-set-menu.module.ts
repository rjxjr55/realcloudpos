import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewSetMenuPage } from './new-set-menu';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';
import { CheckboxModule } from '../../components/check-box/check-box.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NewSetMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(NewSetMenuPage),
    AlertHeaderComponentModule,
    NumpadModule,
    CheckboxModule,
    TranslateModule.forChild()
  ],
  exports: [
    NewSetMenuPage
  ]
})
export class NewSetMenuPageModule {}
