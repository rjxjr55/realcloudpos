import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Toast } from 'ionic-angular';
import { Log } from '../../services/log';

@IonicPage()
@Component({
  selector: 'page-stat-menu-daily',
  templateUrl: 'stat-menu-daily.html',
})
export class StatMenuDailyPage {
  title: string = "상품별 매출 조회";
  startDate: any;
  endDate: any;
  data: any[] = [];
  sumData: any[] = [0, 0, 0, 0];
  total: number = 0;
  statFilter: number = 0;
  pageNo: number = 1;

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private log: Log, private viewCtrl: ViewController, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatMenuDailyPage');
    this.startDate = this.navParams.get('startDate');
    this.endDate = this.navParams.get('endDate');
    this.getSalesMenu();
  }

  getSalesMenu() {
    this.sumData = [0, 0, 0, 0];
    this.log.salesMenu.get(this.startDate, this.endDate, this.statFilter)
      .subscribe(
      data => {
        if (data.json().list.length == 0) {
          this.data = [];
        } else {
          data.json().list.forEach(element => {
            this.data.push({
              menuName: element.menuName,
              menuCNT: element.menuCNT,
              payAmt: element.payAmt,
              payTax: element.payTax,
              payTotal: element.payTotal,
            })
          });
          for (var i = 0; this.data.length > i; i++) {
            this.sumData[0] += this.data[i]['menuCNT'];
            this.sumData[1] += this.data[i]['payAmt'];
            this.sumData[2] += this.data[i]['payTax'];
            this.sumData[3] += this.data[i]['payTotal'];
          }
        }
        console.log(this.data);
      },
      err => {
        this.data = [];
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  // doInfinite() {
  //   this.pageNo++;
  //   this.getSalesMenu(this.pageNo);
  // }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
