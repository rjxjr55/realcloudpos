import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatMenuDailyPage } from './stat-menu-daily';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    StatMenuDailyPage,
  ],
  imports: [
    IonicPageModule.forChild(StatMenuDailyPage),
    AlertHeaderComponentModule,
  ],
  exports: [
    StatMenuDailyPage
  ]
})
export class StatMenuDailyPageModule {}
