import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderPage } from './order';
import { SquareModule } from '../../directives/square/sqare.module';
import { CheckboxModule } from '../../components/check-box/check-box.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrderPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderPage),
    SquareModule,
    CheckboxModule,
    TranslateModule.forChild()
  ],
  exports: [
    OrderPage
  ]
})
export class OrderPageModule {}
