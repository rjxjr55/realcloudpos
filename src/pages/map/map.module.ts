import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    MapPage,
  ],
  imports: [
    IonicPageModule.forChild(MapPage),
    AlertHeaderComponentModule
  ],
  exports: [
    MapPage
  ]
})
export class MapPageModule {}
