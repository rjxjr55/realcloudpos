import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

declare var naver;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  pageTitle: string = "매장 위치 지도 설정";
  markerImg: string = "assets/images/store_setting/marker.png";
  //marker: any;

  lat:any = 0;
  lng:any = 0;
  data = { lat: this.lat, lng: this.lng };

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.data.lat = this.navParams.get('lat');
    this.data.lng = this.navParams.get('lng');

    this.loadMap();
  }

  loadMap() {
    var mapDiv = document.getElementById('map');
    var latLng = new naver.maps.LatLng(this.data.lat, this.data.lng);

    console.log(latLng);

    var mapOptions = {
      center: latLng,
      zoom: 11
    };

    var map = new naver.maps.Map(mapDiv, mapOptions);

    var marker = new naver.maps.Marker({
      position: latLng,
      map: map
    });

    naver.maps.Event.addListener(map, 'click', function(e) {
      marker.setPosition(e.coord);
    });

    naver.maps.Event.addListener(marker, 'mousedown', (e) => {
      console.log('click marker');
      this.data.lat = marker.getPosition().lat();
      this.data.lng = marker.getPosition().lng();

      this.viewCtrl.dismiss(this.data);
    });
  }

  refresh() {
    this.loadMap();
  }

  closeModal() {
    this.viewCtrl.dismiss(this.data);
  }
}
