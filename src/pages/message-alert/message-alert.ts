import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-message-alert',
  templateUrl: 'message-alert.html',
})
export class MessageAlertPage {
  title: string = "";
  message: string = "";
  color: string = 'red';
  duration: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.message = this.navParams.get('message');
    if (this.navParams.get('color')) {
      this.color = this.navParams.get('color');
    }
    if (this.navParams.get('duration')) {
      this.duration = this.navParams.get('duration');
      setTimeout(() => {
        this.viewCtrl.dismiss();
      }, this.duration*1000);
    }

    console.log(this.color);
  }

  close(ev) {
    this.viewCtrl.dismiss();
  }
}
