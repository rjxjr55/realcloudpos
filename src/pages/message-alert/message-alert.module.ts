import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageAlertPage } from './message-alert';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    MessageAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageAlertPage),
    AlertHeaderComponentModule
  ],
  exports: [
    MessageAlertPage
  ]
})
export class MessageAlertPageModule {}
