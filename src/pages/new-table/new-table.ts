import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Setting } from '../../services/setting';

@IonicPage()
@Component({
  selector: 'page-new-table',
  templateUrl: 'new-table.html',
})
export class NewTablePage {
  title: string = '테이블 추가';
  item: any = {};
  tables: Array<any> = [];
  floorNo: number = -1;
  rectChecked: boolean = false;
  circleChecked: boolean = false;
  tableNo: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private setting: Setting, private event: Events) {
  }

  ionViewDidLoad() {
    this.item = this.navParams.get('item');
    this.tables = this.navParams.get('tables');
    this.floorNo = this.navParams.get('floorNo');

    this.tables = this.tables.filter(
      (item) => {
        if(this.item)
          return item.id && this.item.id != item.id && item.floorId == this.floorNo;
        else
          return item.id && item.floorId == this.floorNo;
      }
    );
    console.log(this.tables);
  
    if(this.item && this.item.id) {
      this.title = '테이블 수정';
      this.tableNo = this.item.name;
      if(this.item.dispShape == 0)
        this.rectChecked = true;
      else
        this.circleChecked = true;
    }
    else {
      this.pm('+');
      this.rectChecked = true;
    }
  }

  post() {
    this.setting.table.post(this.floorNo, "" + this.tableNo, 0, this.rectChecked ? '0' : '1')
    .subscribe(
      data => {
        console.log('테이블 추가 성공');
        this.event.publish('reloadTable');
        this.close();
      },
      error => {

      }
    );
  }

  put() {
    this.setting.table.put(this.item.id, "" + this.tableNo, 0, this.item.dispOrder, this.rectChecked ? '0' : '1')
    .subscribe(
      data => {
        console.log('테이블 수정 성공');
        this.event.publish('reloadTable');
        this.close();
      },
      error => {

      }
    );
  }

  pm(val) {
    var temp = this.tableNo;

    if(val == '+') {
      do {
        this.tableNo++;
      } while (this.isDupulicate(this.tableNo));
    }

    else if(val == '-') {
      while(1) {
        temp--;

        if(temp < 1)
          return ;

        if(!this.isDupulicate(temp) && temp > 0) {
          this.tableNo = temp;
          break;
        }
      }
    }
  }

  isDupulicate(val) {
    var arr = this.tables.filter(
      (item) => {
        return val == +item.name;
      }
    )
    
    return arr.length > 0 ? true : false;
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
