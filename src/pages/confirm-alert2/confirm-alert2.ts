import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ToastController, Toast, AlertController } from 'ionic-angular';
// import { timer } from 'rxjs/observable/timer';
// import { Observable } from 'rxjs/Observable';
// import { PACKAGE_NAME } from '../../config';
import { TranslateService } from '@ngx-translate/core';
import { orderNum } from '../../services/orderNum';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';

// declare const chrome;
// declare const GigagenieApi;

@IonicPage()
@Component({
  selector: 'page-confirm-alert2',
  templateUrl: 'confirm-alert2.html',
})
export class ConfirmAlert2Page {
  isQR: boolean=false;
  convertName: any;
  title: string = "";
  label_big: string = "";
  label_payType: string = "";
  label_payAmt: number = 0;
  installmentMm: number = 0;
  isPay: boolean = false;
  socketID: number = 1;
  isCompleted: boolean = false;
  subscription;
  terminal_res;
  cardPayInfo = {
    payNo: 0,
    cardID: "0000-0000-0000-000",
    cardType: "신용",
    cardName: "삼성골드",
    cardCompany: "삼성카드",
    installmentMm: "0",
    cardYYMM: "00-00",
    approvalAmt: 0,
    approvalNo: Math.ceil(Math.random() * 1000),
    approvalDate: '',
    terminalID: "1234",
    terminalStoreID: "181",
    tranID: 123456,
    terminalYn: false,
    isCompleted: false,
    orderNo: null
  }

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public events: Events, public toastCtrl: ToastController, private tran:TranslateService, public orderNum: orderNum, private cat: CatProtocolProvider ,private alertCtrl: AlertController) {
    
  }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.label_big = this.navParams.get('label_big');
    this.label_payType = this.navParams.get('label_payType');
    this.label_payAmt = this.navParams.get('label_payAmt');
    this.installmentMm = this.navParams.get('installmentMm');
    this.convertName = this.navParams.get('convertName');
    this.isQR=this.navParams.get('isQR');
    // this.nativeStorage.getItem('socketId')
    // .then(
    //   (data) => {
    //     console.log(data.id);
    //     this.socketID = data.id;
    //   }
    // );


    if (this.navParams.get('isQR')) {
      this.isPay = this.navParams.get('isPay');
      console.log('go inside');
      // this.printTest();
    }
    else{
      this.isPay = this.navParams.get('isPay');
      this.payTerminalCheck();
    }
  }


  ionViewDidLeave() {
    // this.cardPayInfo.terminalYn = false;
    // this.cardPayInfo.isCompleted = false;
    this.events.unsubscribe('pay:cardInfo');
  }

  payTerminalCheck() {
    this.cat.requestPay(0, this.label_payAmt, this.installmentMm, this.convertName)
    .then((res: any)=>{
      this.cardPayInfo.terminalYn = true;
      this.cardPayInfo.isCompleted = true;
      this.cardPayInfo.orderNo = res.orderNo;
      this.cardPayInfo.approvalNo = res.approvalNo;
      this.cardPayInfo.approvalDate = res.approvalDate;
      this.events.publish('pay:cardInfo', this.cardPayInfo);
      this.viewCtrl.dismiss({res: true});
    })
    .catch(_=>{
      this.tran.get('pay_failed')
      .subscribe(title=>{
        if(_ == -1) {
          _ = this.tran.get('check_connect')
          .subscribe(lang=>{this.alert(title, lang)});
        }else{
          this.alert(title, _);
        }
      })
      
      this.cardPayInfo.terminalYn = true;
      this.cardPayInfo.isCompleted = false;
      this.events.publish('pay:cardInfo', this.cardPayInfo);
      this.viewCtrl.dismiss({res: false});
    });
  }

  alert(title,msg) {
    this.tran.get('yes')
    .subscribe(btn=>{
      this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: [btn]
      })
      .present();
    })
  }

  getLangCode(lang) {
    switch (lang) {
      case "ko":
      return 0;
      case "en":
      return 1;
      case "cn":
      return 2;
    }
  }

  saveTerminalInfo(terminal_res) {
    var charinfo = [];
    var temp = '';

    terminal_res.forEach((element, idx) => {
      for (let index = 0; index < element.length; index += 2) {
        temp += this.hex2char(element[index] + element[index + 1] + '');
        if (index == element.length - 2) {
          charinfo.push(temp);
          temp = '';
        }
      }
    });

    return charinfo;
  }

  setCardInfo(decData) {
    console.log(decData)
    this.cardPayInfo = {
      payNo: 0,
      cardID: decData[2],
      cardType: "",
      cardName: "",
      cardCompany: this.terminal_res[13].slice(8, this.terminal_res[13].length),
      // cardCompany: '',
      installmentMm: decData[6],
      cardYYMM: "00-00",
      approvalAmt: decData[3],
      approvalNo: decData[7],
      terminalID: decData[12].slice(0, 9),
      terminalStoreID: decData[11],
      tranID: decData[12].slice(10, 13),
      terminalYn: false,
      isCompleted: false,
      orderNo: null,
      approvalDate: ''
    }
    this.events.publish('cardInfo',this.cardPayInfo);
  }




  decToHexArray(a: number) {
    var s = a.toString();
    var result = [];

    for (let i = 0; i < s.length; i++) {
      result.push(parseInt('0x3' + s[i]));
    }
    return result;
  }


  byteArrayToHexString(a) {
    var s = '';

    if (a instanceof ArrayBuffer) {
      a = new Uint8Array(a);
    }

    if (!(a instanceof Uint8Array)) {
      console.log('Expecting Uint8Array. Got ' + a);
      return s;
    }

    a.forEach((i) => {
      s += this.toHex(i);
    });
    return s;
  }

  // one byte unsigned int to padded writeData string
  toHex(i) {
    if (i < 0 || i > 255) {
      throw ('Input must be between 0 and 255. Got ' + i + '.');
    }
    return ('00' + i.toString(16)).substr(-2) + ' ';
  }

  hex2char(hex) {
    // converts a single hex number to a character
    // note that no checking is performed to ensure that this is just a hex number, eg. no spaces etc
    // hex: string, the hex codepoint to be converted
    var result = '';
    var n = parseInt(hex, 16);
    if (n <= 0xFFFF) {
      result += String.fromCharCode(n);
      // console.log(1111111111111111111)
    }
    else if (n <= 0x10FFFF) {
      // console.log(22222222222222222222)
      n -= 0x10000
      result += String.fromCharCode(0xD800 | (n >> 10)) + String.fromCharCode(0xDC00 | (n & 0x3FF));
    }
    else { result += 'hex2Char error: Code point out of range: '; }
    return result == undefined ? '' : result;
  }


  close(status) {
    if (status == 1) {
      this.viewCtrl.dismiss({res: true});
    }
    else {
      this.viewCtrl.dismiss({res: false});
      this.cat.cancel().then();
    }
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

}
