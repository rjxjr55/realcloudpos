import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-push-detail',
  templateUrl: 'push-detail.html',
})
export class PushDetailPage {
  pageTitle: string = "푸시 상세보기";
  detail: any = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PushDetailPage');
    
    this.detail = this.navParams.get('whichDetail');
    if(this.detail.pushType =="1")
    this.detail['pushTypeString'] ='전체 고객';
    else
    this.detail['pushTypeString'] ='단골 고객';
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  nl2br(text) {
    return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
}

}
