import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PushDetailPage } from './push-detail';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    PushDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PushDetailPage),
    AlertHeaderComponentModule
  ],
  exports: [
    PushDetailPage
  ]
})
export class PushDetailPageModule {}
