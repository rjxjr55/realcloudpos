import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ToastController, Toast } from 'ionic-angular';
import { Membership } from '../../services/membership';

@IonicPage()
@Component({
  selector: 'page-stamp-set',
  templateUrl: 'stamp-set.html',
})
export class StampSetPage {
  title: string = '스탬프 설정 등록';
  stampSavingAmt: number;
  stampText: string = "";
  type: string = 'set';
  stampUnit: number;
  exchangeText = "";
  attrItem: any = {};
  add : boolean = false;

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private membership: Membership, public toastCtrl: ToastController, ) {
  }

  ionViewDidLoad() {
    if (this.navParams.get('stampSavingAmt')) {
      this.stampSavingAmt = this.navParams.get('stampSavingAmt');
      this.stampText = this.navParams.get('stampText');
    }
    this.type = this.navParams.get('type');
    if (this.type == 'attr') {
      if (this.navParams.get('attrItem')) {
        this.attrItem = this.navParams.get('attrItem');
        this.stampUnit = this.attrItem.stampUnit;
        this.exchangeText = this.attrItem.exchangeText;
        this.title = '스탬프 속성 수정';
        this.add = false;
      } else {
        this.title = '스탬프 속성 등록';
        this.add = true;
      }
    }


  }

  modifyStampSet() {
    this.membership.stampSet.post(this.stampText, this.stampSavingAmt).subscribe(
      data => {
        this.toast("스탬프 설정을 성공적으로 등록 했습니다.");
        this.close();
      },
      err => {
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }

  addStampAttr() {
    this.membership.stampAttr.post(this.stampUnit, this.exchangeText).subscribe(
      data => {
        this.toast("스탬프 속성을 성공적으로 등록 했습니다.");
        this.close();
      },
      err => {
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else if(err.json().message == "duplicated") this.toast("기존에 있는 속성입니다. 속성을 수정해 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }

  modifyStampAttr() {
    this.membership.stampAttr.put(this.stampUnit, this.exchangeText).subscribe(
      data => {
        this.toast("스탬프 속성을 성공적으로 수정 했습니다.");
        this.close();
      },
      err => {
        if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
        else if(err.json().message == "empty") this.toast("등록화면에서 새로운 스탬프 속성을 만들어 주세요.");
        else this.toast("오류입니다. 다시 시도해 주세요.");
      }
    );
  }

  close() {
    this.viewCtrl.dismiss();
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }
}
