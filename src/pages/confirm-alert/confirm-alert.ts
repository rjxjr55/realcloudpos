import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-confirm-alert',
  templateUrl: 'confirm-alert.html',
})
export class ConfirmAlertPage {
  title: string = "";
  label_big: string = "";
  label_small: string = "";
  label_small2: string = "";
  type: number = 0; //0: title and content; 1: icon and content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.label_big = this.navParams.get('label_big');
    this.label_small = this.navParams.get('label_small');
    this.type = this.navParams.get('type');
    this.label_small2= this.navParams.get('label_small2');
  }

  close(status) {
    if(status == 1) {
      this.viewCtrl.dismiss({res: true});
    }
    else {
      this.viewCtrl.dismiss({res: false});
    }
  }
}
