import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, ModalController, Toast, ToastController } from 'ionic-angular';
import { Apollo, QueryRef} from 'apollo-angular';
import gql from 'graphql-tag';
import { NetworkStatusService } from '../../services/network-status.service';
import { Setting } from '../../services/setting';
import * as _ from 'lodash';
import { LoginSession } from "../../services/login.session";
import { TranslateService } from '@ngx-translate/core';
// import { User } from '../../services/user';
//import { IS_GENIE } from '../../config';

declare let $: any;

const Category = gql`
  query {
    getStore {
      id
      category {
        id
        name{
          ko
          en
          cn
          ja
        }
        menu {
          id
          menuNo
          name{
            ko
            en
            cn
            ja
          }
          categoryNo
          saleAmt
          stampUseYN
          takeoutDCAmt
          menuBgColor
          dispOrder
          menuType
          stockCNT
          stockSaleYN
          soldOutYN
          thumbnailS
          thumbnailM
          thumbnailL
          unitPrice
          menuCode
          startDt
          endDt
          taxType
          useYN
          subList {
            id
            subMenuNo
            menuText
            menuCNT
          }
        }
      }
    }
  }
`;

@IonicPage()
@Component({
  selector: 'page-order-menu-list',
  templateUrl: 'order-menu-list.html',
})

export class OrderMenuListPage {
  @ViewChild(Content) content: Content;

  categories: Array<any> = [];
  selectedCategoryIdx: number = 0;
  selectedMenuNo: number = 0;
  selectedOrderIdx:number = -1;
  orderDetail: Array<any> = [];
  bOnline: boolean = true;

  editMode: boolean = false;

  menuObs: QueryRef<any>;
  menuSub: any;

  toastInstance: Toast = null;
  lang:string='ko';

  curUser;
  isModalOpened: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, private apollo: Apollo, 
    public networkStatus: NetworkStatusService, private modal: ModalController, private setting: Setting, private toastCtrl: ToastController, 
    private loginSession: LoginSession, private tran:TranslateService) {
    networkStatus.networkStatus$.subscribe(res=>{
      this.bOnline = res;
    });
  }

  storename: any=this.loginSession.getInfo().storeName;

  ionViewDidLoad() {
    this.orderDetail = this.navParams.get('orderDetail');
    console.log('menu ionViewDidLoad');
  }
  
  ionViewDidEnter() {
    // this.events.publish('layout:header', '영업', true, false, true, true);
    console.log('menu ionViewDidEnter');
    this.events.publish('layout:header', '영업', true, false, true);
  }

  ionViewWillEnter() {
    console.log('menu ionViewWillEnter');
    
    this.events.subscribe('menu-list:refresh', () => {
      this.refresh();
    });
    this.events.subscribe('modal:new-menu', () => {
      this.newMenu();
    });
    this.events.subscribe('modal:new-set-menu', () => {
      this.newSetMenu();
    });
    this.events.subscribe('lang:changed', (lang) => {
      this.lang=lang;
      localStorage.setItem('lang', lang);
      
    });

    if((localStorage.getItem('lang')=='ko')||(localStorage.getItem('lang')=='en')||(localStorage.getItem('lang')=='cn'))
    {
          this.lang=localStorage.getItem('lang');
    }

    this.bringData();

  }


  bringData(){
    this.menuObs = this.apollo.watchQuery({
      query: Category,
      fetchPolicy: "network-only"
    });
    this.menuSub = this.menuObs
    .valueChanges.subscribe(
      (res: any) =>{
        if (res.data) {
          var categories = [];
          categories = _.cloneDeep(res.data.getStore.category ? res.data.getStore.category : []);
          if(categories.length > 0) {
            var tempAllCategory = [];
            for(let category of categories) {
              tempAllCategory = tempAllCategory.concat(category.menu);
            }
            // 전체 카테고리 메뉴를 dispOrder 순으로 정렬
            tempAllCategory.sort((a, b) => {
              return a.dispOrder - b.dispOrder;
            });
            tempAllCategory.forEach(element => {
              if(this.orderDetail && this.orderDetail.length > 0) {
                this.orderDetail.forEach(item => {
                  if((item.menuNo == element.menuNo) && element.stockSaleYN == 1) {
                    element.stockCNT += item.itemCNT;
                  }
                });
              }
            });
            this.tran.get('category').subscribe((res:string)=>{
              categories.unshift({menu: tempAllCategory, name: res});
            })
          }

          this.categories = categories;
          this.content.resize();
          this.events.publish('layout:menu-filter-height', this.content.getContentDimensions().contentTop);
          this.initDroppable();
        }
      }
    );
    this.changeCategory(0);
  }

  ionViewWillLeave() {
    console.log('menu ionViewWillLeave');
    this.menuSub.unsubscribe();
    this.events.unsubscribe('menu-list:refresh');
    this.events.unsubscribe('modal:new-menu');
    this.events.unsubscribe('modal:new-set-menu');
    this.events.unsubscribe('lang:changed');
    this.saveEdited();
  }

  refresh() {
    this.menuObs.refetch();
  }

  changeCategory(idx) {
    if(this.editMode && (idx == this.selectedCategoryIdx) && idx != 0)
      this.editCategory(idx);
    else {
      this.selectedCategoryIdx = idx;
      this.initDroppable();      
    }
  }

  initDroppable() {
    setTimeout(() => {
      // $('.draggable').draggable("option", "revert", false);

      if(this.editMode) {
        $('.draggable').draggable({ revert: true, delay: 300 });
        $('.droppable').droppable({
          drop: (event, ui) => {
            
            var dragIndex = $(event.srcElement).attr("value");
            var dropIndex = $(event.target).attr("value");

            var dragItem = _.cloneDeep(this.categories[this.selectedCategoryIdx].menu[dragIndex]);
            dragItem.menuNo = dragItem.id;
            var dropItem = _.cloneDeep(this.categories[this.selectedCategoryIdx].menu[dropIndex]);
            dropItem.menuNo = dropItem.id;

            var temp = dragItem.dispOrder;
            dragItem.dispOrder = dropItem.dispOrder;
            dropItem.dispOrder = temp;

            this.editMenu(dragItem)
            .subscribe(
              data => {
                console.log('메뉴 수정 성공');
                this.editMenu(dropItem)
                .subscribe(
                  data => {
                    console.log('메뉴 수정 성공');
                    this.refresh();
                  },
                  error => {
                  }
                );
              },
              error => {
              }
            );
          }
        });
      }
    }, 0);
  }

  editMenu(item) {
    return this.setting.menu.put(item);
  }

  editCategory(idx) {
    let modal = this.modal.create('OneInputAlertPage', {title: '카테고리 수정', label: '카테고리명', 'input_str': this.categories[idx].name.ko}, {cssClass: 'message-modal', enableBackdropDismiss: false});
    modal.onDidDismiss(data => {
      if(data.val.length > 0) {
        this.setting.menuCategory.put(this.categories[idx].id, data.val, this.categories[idx].dispOrd,'ko')
        .subscribe(
          data => {
            console.log('카테고리 수정 완료');
            this.refresh();
          }
        );
      }
    });
    modal.present();
  }

  deleteCategory(idx) {
    console.log(this.categories[idx]);
    
    let modal = this.modal.create('ConfirmAlertPage', {title: '다음 카테고리를 삭제하시겠습니까?', 'label_big': this.categories[idx].name.ko, 'label_small': '삭제하시면 해당 카테고리 메뉴들이 다 삭제됩니다. 복구되지 않습니다.', type: 0}, {cssClass: 'message-modal', enableBackdropDismiss: false});
    modal.onDidDismiss(data => {
      if(data.res) {
        this.setting.menuCategory.delete(this.categories[idx].id)
        .subscribe(
          data => {
            console.log('카테고리 삭제 완료');
            this.selectedCategoryIdx = 0;
            this.refresh();
          },
          error=>{         
          });
      }
    });
    modal.present();
  }

  addOrder(item) {
    if(this.editMode && item.menuType == "1")
      this.newMenu(item);
    else if(this.editMode && item.menuType == "2")
      this.newSetMenu(item);
    else {
      item.menuName = item.name[this.lang];
      this.events.publish('order:add', item);
    }
  }

  intoEditMode() {
    this.editMode = true;
    this.events.publish('layout:backdrop', this.editMode);
    this.initDroppable();
  }

  saveEdited() {
    this.editMode = false;
    this.events.publish('layout:backdrop', this.editMode);
    this.refresh();
  }

  newMenu(item = null) {
    console.log(item);
    if(!this.isModalOpened){
      this.isModalOpened = true;
      let modal = this.modal.create('NewMenuPage', {categories: this.categories, item: (item ? item : null), selectedCategoryIdx: this.selectedCategoryIdx}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
      modal.onDidDismiss(
        () => {
          this.refresh();
          console.log('리프레시');
          this.isModalOpened = false;
        }
      );
      modal.present();
    }
  }

  newSetMenu(item = null) {
    console.log(item);
    if(!this.isModalOpened){
      this.isModalOpened = true;
      let modal = this.modal.create('NewSetMenuPage', {categories: this.categories, item: (item ? item : null), selectedCategoryIdx: this.selectedCategoryIdx}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
      modal.onDidDismiss(
        () => {
          this.refresh();
          console.log('리프레시');
          this.isModalOpened = false;
        }
      );
      modal.present();
    }
  }

  deleteMenu(item) {
    console.log(item);
    if(!this.isModalOpened){
      this.isModalOpened = true;
      let modal = this.modal.create('ConfirmAlertPage', { title: '메뉴 삭제', label_small: '\''+item.name.ko + '\'을(를) 삭제하시겠습니까?', type: 1 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
      modal.onDidDismiss(data => {
        this.isModalOpened = false;
        if (data.res) {
          this.setting.menu.bin.put(item.id)
          .subscribe(
            data => {
              console.log('메뉴삭제완료');
              alert('메뉴삭제완료');
              this.refresh();
            },
            error => {
              console.log(error);
              alert('메뉴삭제실패');
            }
          );
        }
      });
      modal.present();
    }
  }

  stockWarn(item) {
    if(!this.isModalOpened){
      this.isModalOpened = true;
      let modal = this.modal.create('StockWarnPage', {item: item}, {cssClass: 'medium-modal', enableBackdropDismiss: false});
      modal.onDidDismiss(data => { this.isModalOpened = false; });
      modal.present();
    }
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }
}
