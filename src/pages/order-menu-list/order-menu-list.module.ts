import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderMenuListPage } from './order-menu-list';
import { SquareModule } from '../../directives/square/sqare.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrderMenuListPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderMenuListPage),
    SquareModule,
    TranslateModule.forChild()
  ],
  exports: [
    OrderMenuListPage
  ]
})
export class OrderMenuListPageModule {}
