import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageCropperPage } from './image-cropper';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    ImageCropperPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageCropperPage), AlertHeaderComponentModule
  ],
  exports: [
    ImageCropperPage
  ]
})
export class ImageCropperPageModule {}
