import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ToastController, Toast } from 'ionic-angular';

declare const Cropper;
/**
 * Generated class for the ImageCropperPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-image-cropper',
  templateUrl: 'image-cropper.html',
})
export class ImageCropperPage {
  @ViewChild('image') imageElement: ElementRef;

  pageTitle: string = "이미지 크기 조절";

  imageData: any;
  cropper: any;
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public events: Events, public toastCtrl: ToastController) {
    this.imageData = navParams.get("imageData");
  }

  ionViewDidLoad() {
    this.cropper = new Cropper(this.imageElement.nativeElement, {
      aspectRatio: this.navParams.get('aspectRatio'),
      crop: function (e) { }
    });
    this.events.publish('log', 'ImageCropperPage')
  }

  cropComplete() {
    this.viewCtrl.dismiss(this.cropper.getCroppedCanvas().toDataURL('image/jpeg'));
    this.toast("사진을 가져왔습니다.");
  }

  closeModal() {
    this.viewCtrl.dismiss();
    this.toast("사진 가져오기를 취소하였습니다.");
  }

  toast(text) {

    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

}
