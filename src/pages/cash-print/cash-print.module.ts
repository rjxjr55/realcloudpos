import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CashPrintPage } from './cash-print';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';

@NgModule({
  declarations: [
    CashPrintPage,
  ],
  imports: [
    IonicPageModule.forChild(CashPrintPage),
    AlertHeaderComponentModule,
    NumpadModule,
  ],
  exports: [
    CashPrintPage
  ]
})
export class CashPrintPageModule {}
