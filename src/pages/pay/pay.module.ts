import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayPage } from './pay';
import { NumpadModule } from "../../modules/Numpad/numpad.module";
import { CheckboxModule } from '../../components/check-box/check-box.module';
import { MileageComponentModule } from "../../components/mileage/mileage.module";

@NgModule({
  declarations: [
    PayPage,
  ],
  imports: [
    IonicPageModule.forChild(PayPage),
    NumpadModule,
    CheckboxModule,
    MileageComponentModule,
  ],
  exports: [
    PayPage
  ]
})
export class PayPageModule { }
