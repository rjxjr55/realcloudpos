import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-one-input-alert',
  templateUrl: 'one-input-alert.html',
})
export class OneInputAlertPage {
  title: string = "";
  label: string = "";
  input_str: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.label = this.navParams.get('label');
    this.input_str = this.navParams.get('input_str');
  }

  close(status: number) {
    if (status == 1) {
      if (this.input_str)
        this.viewCtrl.dismiss({ val: this.input_str });
    }
    else {
      this.viewCtrl.dismiss({ val: "" });
    }
  }
}
