import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OneInputAlertPage } from './one-input-alert';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';

@NgModule({
  declarations: [
    OneInputAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(OneInputAlertPage),
    AlertHeaderComponentModule
  ],
  exports: [
    OneInputAlertPage
  ]
})
export class OneInputAlertPageModule {}
