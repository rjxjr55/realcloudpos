import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, ToastController, Toast, LoadingController } from 'ionic-angular';
import { User } from '../../services/user';
import { LoginSession } from '../../services/login.session';
import { Pay } from '../../services/pay';
import { Review } from '../../services/review';
import { Camera } from "@ionic-native/camera";
import { ImageProvider } from "../../providers/image/image-provider";
import { Membership } from '../../services/membership';
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: 'page-membership',
  templateUrl: 'membership.html',
})
export class MembershipPage {
  pushContents: string;
  userList: Array<any> = [];
  selectedUser: any = null;
  whichtab: any = 1;
  saveOrNot: boolean = false;
  cost: any = 0;
  search: string = "";

  checked: boolean = false;

  userInfo: any = {};
  pageTitle: string = "";
  which: any = 0;
  storeInfo: any = this.loginSession.getInfo();

  beforemileage: any = 0;
  aftermileage: any = 0;
  saveRatio: any = 0;
  beforeStamp: any = 0;
  afterStamp: any = 0;
  stampCnt: any = 0;

  checkedBox: any = -1;
  checkOrNot: Array<boolean> = [];
  selectedNumpad: any = 1;

  listOrDetail: boolean = true;
  reviewList: Array<any> = [];
  selectedReview: any = null;
  commentList: Array<any> = [];
  reviewCnt: number = 0;
  averagePoint: number = 0;
  commentTxt: string = null;
  commenTotal: number = 0;
  openMileageComponent: boolean = false;

  total: number = 0;
  pageNo: number = 1;
  commentPageNo: number = 1;
  today = new Date();
  endDate = this.today;
  startDate = new Date(this.today.getTime() - (7 * 24 * 60 * 60 * 1000), );
  pushImg: any;

  pushTitle: string = "";
  sendType: number = 2;
  pushList: Array<any> = [];
  mileageOrStamp: number;

  private toastInstance: Toast;
  pushSendButtun: boolean = false;
  pushSendStatus: boolean = false;

  @ViewChild('inputAmt') inputAmt;

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, private user: User, private modalCtrl: ModalController, private loginSession: LoginSession, private pay: Pay, private review: Review, public toastCtrl: ToastController, private imageProvider: ImageProvider, private membership: Membership, public loadingCtrl: LoadingController, private camera: Camera) {
  }

  ionViewDidLoad() {
    this.events.publish('layout:header', '멤버십', true, false, true);
    this.getUser();
    if (this.loginSession.getInfo().mileageStampOpt == 1) {
      this.mileageOrStamp = 1;
      this.pageTitle = '마일리지 적립';
    }
    else {
      this.mileageOrStamp = 2;
      this.pageTitle = '스탬프 적립';
    }
  }

  getUser(pageNo = 1) {

    if (this.checkedBox != -1)
      this.checkOrNot[this.checkedBox] = false;

    this.user.client(this.search, pageNo)
      .subscribe(
      data => {
        this.total = data.json().total;
        if (pageNo == 1) {
          this.userList = [];
          if (data.json().list.length == 0)
            this.toast(this.loginSession.getInfo().storeName + ' 매장을 단골로 추가한 포항포인트 사용자가 없습니다 ');
          else
            this.toast(this.loginSession.getInfo().storeName + ' 매장을 단골로 추가한 포항포인트 사용자를 가지고 왔습니다. ');
        }
        if (this.total - this.userList.length > 0)
          for (var i = 0; i < data.json().list.length; i++) {
            this.userList.push({
              mileage: data.json().list[i].mileage,
              stampCnt: data.json().list[i].stampCnt,
              tel: data.json().list[i].tel,
              totalPoint: data.json().list[i].totalPoint,
              tranCnt: data.json().list[i].tranCnt,
              userName: data.json().list[i].userName,
              userNo: data.json().list[i].userNo
            });
          }
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
    this.selectedUser = null;
  }

  selectUser(userInfo) {
    this.selectedUser = userInfo;
  }

  showMileageLog(userInfo) {
    let modal = this.modalCtrl.create('MileageLogPage', { userInfo: userInfo }, {
      cssClass: 'mileage-modal'
    });
    modal.present();
  }

  saveMileage(val) {
    if (this.selectedUser != null) {
      if (val == true) {
        this.saveOrNot = val;
        this.userInfo = this.navParams.get('userInfo');

        if (this.mileageOrStamp == 1) {
          this.pageTitle = '마일리지 적립';
          this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
          this.aftermileage = this.selectedUser.mileage + this.beforemileage;
          this.cashOrCharge(1);
        }
        else {
          this.pageTitle = '스탬프 적립';
          this.beforeStamp = this.stampCnt;
          this.afterStamp = this.selectedUser.stampCnt + this.beforeStamp;
        }
        this.events.publish('layout:backdrop', true);
        setTimeout(() => {
          this.inputAmt.focus();
        }, 300);
      } // 켠당
      else if (val == false) {
        this.saveOrNot = val;

        this.which = 0;
        this.cost = 0;
        this.stampCnt = 0;
        this.saveRatio = 0;

        this.events.publish('layout:backdrop', false);
      } // 끈당
    }
    else
      this.toast('포항포인트 사용자를 선택해 주세요!');
  }

  saveComplete() {

    if (this.mileageOrStamp == 1 && this.beforemileage == 0) {
      this.toast('적립 마일리지를 확인해 주세요. ');
      return 0;
    }
    if (this.mileageOrStamp == 2 &&this.beforeStamp == 0) {
      this.toast('적립 스탬프 개수를 확인해 주세요. ');
      return 0;
    }
    this.saveMileage(false);

    if (this.mileageOrStamp == 1) {
      this.modifyMileage(this.selectedUser);
    } //마일리지
    else {
      this.modifyStamp(this.selectedUser);
    } //스탬프
  }

  modifyMileage(selectedUser) {
    this.pay.pointPay.post(selectedUser.tel, selectedUser.userNo, this.cost, 0, 1, this.beforemileage, this.saveRatio).subscribe(
      data => {
        if (data.json().state == "success") {
          let modal = this.modalCtrl.create('SaveMileageContainerPage', { beforemileage: this.beforemileage, aftermileage: this.aftermileage }, { cssClass: 'message-modal' });
          modal.onDidDismiss(data => {
            this.getUser();
          });
          modal.present();
        }
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
    );
  }

  modifyStamp(selectedUser) {
    this.pay.pointPay.post(selectedUser.tel, selectedUser.userNo, this.cost, this.beforeStamp, 1, 0, 0).subscribe(
      data => {
        if (data.json().state == "success") {
          let modal = this.modalCtrl.create('SaveStampContainerPage', { saveOrUse: true,  beforeStamp: this.beforeStamp, afterStamp: this.afterStamp }, { cssClass: 'message-modal' });
          modal.onDidDismiss(data => {
            this.getUser();
          });
          modal.present();
        }
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
    );
  }

  clickSideTab(whichtab) {
    this.whichtab = whichtab;
    this.pageNo = 1;
    this.search = "";
    this.getInfo(whichtab, this.pageNo);
  }

  getInfo(val, pageNo) {
    switch (val) {
      case 1:
        this.getUser(pageNo);
        break;
      case 2:
        this.getReview(pageNo);
        this.listOrDetail = true;
        this.commentPageNo = 1;
        break;
      case 3:
        this.getPushList(pageNo);
        break;
    }
  }

  OnPageChanged() {
    this.pageNo++;
    this.getInfo(this.whichtab, this.pageNo);
  }

  cashOrCharge(which) {
    this.which = which;

    if (which == 1) { // card
      this.saveRatio = this.storeInfo.mileageSaveCard;
    }
    else { //cash
      this.saveRatio = this.storeInfo.mileageSaveCash;
    }

    this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
    this.aftermileage = this.selectedUser.mileage + this.beforemileage;
  }

  pm(val) {
    if (this.mileageOrStamp == 1) {
      if (val == '+')
        this.saveRatio++;
      else if (val == '-' && this.saveRatio > 0)
        this.saveRatio--;

      this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
      this.aftermileage = this.selectedUser.mileage + this.beforemileage;
    }
    else {
      if (val == '+')
        this.stampCnt++;
      else if (val == '-' && this.stampCnt > 0)
        this.stampCnt--;

      this.beforeStamp = this.stampCnt;
      this.afterStamp = this.selectedUser.stampCnt + this.beforeStamp;
    }
  }

  valueChange(val) {
    if (this.mileageOrStamp == 1) {
      this.saveRatio = val;

      this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
      this.aftermileage = this.selectedUser.mileage + this.beforemileage;
    }
    else {
      this.stampCnt = val;

      this.beforeStamp = this.stampCnt;
      this.afterStamp = this.selectedUser.stampCnt + this.beforeStamp;
    }
  }

  costChange(val) {
    this.cost = val;

    this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
    this.aftermileage = this.selectedUser.mileage + this.beforemileage;
  }

  whichNumpad(val) {
    this.selectedNumpad = val;
  }


  getReview(pageNo) {
    this.review.reviewDetail.get(pageNo, 10, '').subscribe(
      data => {
        this.total = data.json().total;

        if (pageNo == 1) {
          this.reviewList = [];
          if (data.json().list.length == 0)
            this.toast(' 매장 리뷰가 없습니다.');
          else
            this.toast('매장 리뷰를 가지고 왔습니다. ');
        }

        if (this.total - this.reviewList.length >= 0)
          for (var i = 0; i < data.json().list.length; i++) {
            this.reviewList.push({
              commentCNT: data.json().list[i].commentCNT,
              estimationPoint: data.json().list[i].estimationPoint,
              imageCNT: data.json().list[i].imageCNT,
              imageURL: data.json().list[i].imageURL,
              joined: data.json().list[i].joined,
              likeCNT: data.json().list[i].likeCNT,
              publishDate: data.json().list[i].publishDate,
              modifiedDate: data.json().list[i].modifiedDate,
              readCNT: data.json().list[i].readCNT,
              reviewNo: data.json().list[i].reviewNo,
              reviewTxt: data.json().list[i].reviewTxt,
              status: data.json().list[i].status,
              storeName: data.json().list[i].storeName,
              storeNo: data.json().list[i].storeNo,
              thumbnailURL: data.json().list[i].thumbnailURL,
              userName: data.json().list[i].userName,
              userNo: data.json().list[i].userNo
            });
          }

        this.averagePoint = data.json().totalPoint / this.total;
        this.averagePoint.toFixed(1);

        if (isNaN(this.averagePoint))
          this.averagePoint = 0;

      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
    );

  }

  getReviewComment(reviewNo, pageNo = 1) {
    this.review.reviewComment.get(reviewNo, pageNo, 5).subscribe(
      data => {
        this.commenTotal = data.json().total;
        if (this.commentPageNo == 1) {
          this.commentList = [];
          if (data.json().list.length == 0)
            this.toast(' 해당 리뷰의 댓글이 없습니다.');
          else
            this.toast('해당 리뷰의 댓글을  가지고 왔습니다. ');
        }
        if (this.commenTotal - this.commentList.length > 0)
          for (var i = 0; i < data.json().list.length; i++) {
            this.commentList.push({
              commentNo: data.json().list[i].commentNo,
              commentTxt: data.json().list[i].commentTxt,
              displayName: data.json().list[i].displayName,
              publishDate: data.json().list[i].publishDate,
              userNo: data.json().list[i].userNo
            });
          }

        console.log(this.commentList);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
    );
  }
  commentPageChange() {
    this.commentPageNo++;
    this.getReviewComment(this.selectedReview.reviewNo, this.commentPageNo);

  }

  drawStar(num) {
    var stars: Array<string> = [];
    var i: number = 0;

    for (i = 0; i < num; i++) {
      stars.push('assets/images/membership/star_small_full.png');
    }
    for (i = 0; i < 5 - num; i++) {
      stars.push('assets/images/membership/star_small_empty.png');
    }

    return stars;
  }

  seeMoreReview(item, i) {
    this.selectedReview = item;
    this.listOrDetail = false;
    this.commentPageNo = 1;
    this.getReviewComment(this.selectedReview.reviewNo);
  }

  writeComment() {
    this.review.writeComment.post(this.selectedReview.reviewNo, this.commentTxt).subscribe(
      data => {
        this.getReviewComment(this.selectedReview.reviewNo);
        this.commentTxt = null;
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
    );
  }

  clickDatePicker(type) {
    let dateModal;

    if (type == 1)
      dateModal = this.modalCtrl.create('DatePickerModalPage', { date: this.startDate }, { enableBackdropDismiss: false, cssClass: 'date-modal' });
    else
      dateModal = this.modalCtrl.create('DatePickerModalPage', { date: this.endDate }, { enableBackdropDismiss: false, cssClass: 'date-modal' });

    dateModal.onDidDismiss(data => {
      if (type == 1) {
        if (this.today < data.date) {
          this.startDate = this.today;
          this.toast('오늘 이후의 날짜는 선택 하실 수 없습니다.');
        } else {
          this.startDate = data.date;
          if (this.startDate > this.endDate)
            this.endDate = this.startDate;
        }
      }
      else {
        if (this.today < data.date) {
          this.endDate = this.today;
          this.toast('오늘 이후의 날짜는 선택 하실 수 없습니다.');
        } else {
          this.endDate = data.date
          if (this.startDate > this.endDate)
            this.startDate = this.endDate;
        }
      }
      this.getPushList();
    });
    dateModal.present();
  }

  attachPic() {
    let options = {
      destinationType: 0,
      encodingType: 0,
      sourceType: 2,
      quality: 50
    };

    this.camera.getPicture(options).then(
      (data) => {
        this.pushImg = "data:image/jpeg;base64," + data;
      }, (error) => {
        this.toast("사진을 선택하지 않았습니다.");
      });
  }

  complete() {
    // var data: Array<any> = [];
    var item: any;

    var filename = this.loginSession.getInfo().storeNo + '_' + (new Date().getTime()) + Math.random() + '.jpg';

    this.imageProvider.s3.put(filename, this.pushImg).subscribe(
      res => {
        console.log(res);
      }
    );

    item = {
      "dispOrder": 0,
      "url": "https://s3.ap-northeast-2.amazonaws.com/hgus3/origin/store/" + filename,
      "thumbnail_l": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/l/store/" + filename,
      "thumbnail_m": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/m/store/" + filename,
      "thumbnail_s": "https://s3.ap-northeast-2.amazonaws.com/hgus3/thumbnail/s/store/" + filename
    };
    console.log(item);

    this.pushImg = item.url;

    this.imageProvider.post(item.url, item.thumbnail_s, item.thumbnail_m, item.thumbnail_l, 0).subscribe(
      res => {
        this.toast("이미지 업로드 성공!");
      }
    );
  }

  pushDetailModal(val) {
    console.log(val.pushNo);
    let modal = this.modalCtrl.create('PushDetailPage', { whichDetail: val }, { cssClass: 'mileage-modal' });
    modal.present();
  }

  sendPush() {
    this.pushSendButtun = true;
    setTimeout(() => {
      if (!this.pushTitle) {
        this.toast('푸시 제목을 입력하세요.');
        return 0;
      }
      else if (!this.pushContents || this.pushContents == '<p><br></p>') {
        this.toast('푸시 내용을 입력하세요.');
        this.pushSendButtun = false;
        return 0;
      }
      else {
        console.log(this.pushTitle);
        console.log(this.pushContents);


        this.pushSendButtun = true;
        let loading = this.loadingCtrl.create({
          content: '푸시를 전송하는 중입니다.',
          spinner: 'circle'
        });
        loading.present();
        this.membership.push.post(this.pushTitle, this.pushContents, 'public', this.sendType, 1).subscribe(
          data => {
            this.toast("푸시를 성공적으로 보냈습니다.");
            this.pushTitle = "";
            this.pushSendButtun = false;
            this.pushSendStatus = true;
            loading.dismiss();
            setTimeout(() => {
              this.pushSendStatus = false;
            }, 500);
            this.getPushList();
          },
          err => {
            if (err == "network") this.toast("인터넷 연결을 확인해 주세요.");
            else this.toast("오류입니다. 다시 시도해 주세요.");
          }
        );
      }
    }, 500);


  }

  getPushList(pageNo = 1) {
    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');
    this.membership.push.get(chooseStartDate, chooseEndDate, this.search, pageNo)
      .subscribe(
      data => {
        // this.pushList = data.json().list;
        this.total = data.json().total;
        if (pageNo == 1) {
          this.pushList = [];
          if (data.json().list.length == 0)
            this.toast(' 푸시가 없습니다. 일자를 다시 조정해 주세요. ');
          else
            this.toast('푸시를 가지고 왔습니다. ');
        }
        if (this.total - this.pushList.length > 0)
          for (var i = 0; i < data.json().list.length; i++) {
            this.pushList.push({
              pushNo: data.json().list[i].pushNo,
              pushTitle: data.json().list[i].pushTitle,
              pushTxt: data.json().list[i].pushTxt,
              receive: data.json().list[i].receive,
              readCnt: data.json().list[i].readCnt,
              sendDttm: datePipe.transform(data.json().list[i].sendDttm, 'yyyy-MM-dd'),
              userNo: data.json().list[i].userNo,
              pushType: data.json().list[i].pushType
            });
          }
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
    this.selectedUser = null;
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

  ionViewWillLeave() {
    this.events.publish('layout:backdrop', false);
  }

  getContentsData(events) {
    this.pushContents = events;
  }

}
