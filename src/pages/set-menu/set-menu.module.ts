import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetMenuPage } from './set-menu';

@NgModule({
  declarations: [
    SetMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(SetMenuPage),
  ],
  exports: [
    SetMenuPage
  ]
})
export class SetMenuPageModule {}
