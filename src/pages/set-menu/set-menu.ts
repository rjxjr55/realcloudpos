import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Toast, IonicPage } from 'ionic-angular';
import { Setting } from '../../services/setting';

@IonicPage()
@Component({
  selector: 'page-set-menu',
  templateUrl: 'set-menu.html'
})
export class SetMenuPage {
  nowdate = new Date();
  endDate = new Date(this.nowdate.getTime() + (30 * 24 * 60 * 60 * 1000));
  menuCategoryList: any;
  menuList: any;
  setMenuList: any = null;
  setMenuTotal: number = 0;
  categoryNo: number = -1;
  subSetMenuList: any;
  selectedSetItem: number = -1;
  selectedSetMenuNo: number = -1;
  taxType: boolean = false;
  useYN: boolean = false;
  stampUseYN: boolean = false;
  soldOutYN: boolean = false;

  //결제 세금 비율
  taxRatio: number = 11;


  item: any = {};

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private setting: Setting, public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetMenuPage');
    this.getSetMenuList(-1);
    this.getMenuCategoryList();
    this.getMenuList(-1);
    this.newSetMenu();
  }

  getMenuCategoryList() {
    this.setting.menuCategory.get()
      .subscribe(
      data => {
        if (data.json() == null)
          this.toast('메뉴 분류가 없습니다. 추가해 주세요!');
        else
          this.menuCategoryList = data.json();
        console.log(this.menuCategoryList);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  getMenuList(menuCategory) {
    this.setting.menu.get(menuCategory, 1)
      .subscribe(
      data => {
        //얘는 오류 처리를 어캐하지  ?
        this.menuList = data.json().list;
        // this.menulTotal = data.json().total;
        console.log(this.menuList);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  getSetMenuList(menuCategory) {
    this.setting.menu.get(menuCategory, 2)
      .subscribe(
      data => {
        if (data.json().list.length == 0)
          this.toast('세트메뉴가 없습니다. 추가해 주세요!');
        else {
          this.setMenuList = data.json().list;
          this.setMenuTotal = data.json().total;
          this.toast('세트메뉴를 가지고 왔습니다.');
        }
        console.log(this.setMenuList);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  selectedSetMenu(item, idx) {
    this.item = item;
    this.item.taxType == "1" ? this.taxType = true : this.taxType = false;
    this.item.useYN == "1" ? this.useYN = true : this.useYN = false;
    this.item.stampUseYN == "1" ? this.stampUseYN = true : this.stampUseYN = false;
    this.item.soldOutYN == "1" ? this.soldOutYN = true : this.soldOutYN = false;

    // 화면에 표시하려고 만든 array 위치 값 
    this.selectedSetItem = idx;
    // 선택된 메뉴의 메뉴 number
    this.selectedSetMenuNo = item.menuNo;
    console.log(this.item);
    this.getSubMenuList();
  }

  getSubMenuList() {
    console.log(this.selectedSetMenuNo);
    this.setting.subSetMenu.get(this.selectedSetMenuNo)
      .subscribe(
      data => {
        if (data.json().list.length == 0) {
          this.toast('서브 메뉴가 없습니다. 추가해 주세요!');
          this.subSetMenuList = null;
        }
        else {
          this.subSetMenuList = data.json().list;
          this.subSetMenuList.forEach(elementset => {
            this.menuList.forEach(element => {
              if (elementset.subMenuNo == element.menuNo) {
                elementset.menuName = element.menuName;
                elementset.saleAmt = element.saleAmt;
              }
            });
          });
        }
        console.log(this.subSetMenuList);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  addSubMenu(item) {
    if (this.subSetMenuList == undefined || this.subSetMenuList == null) {

      var pushData;
      this.menuList.forEach(element => {
        if (item.menuNo == element.menuNo) {
          this.calUnitPrice(item.saleAmt)
          pushData = [{ "menuName": element.menuName, "setMenuNo": this.selectedSetMenuNo, "subMenuNo": item.menuNo, "saleAmt": item.saleAmt, "menuCNT": 1, "menuText": "", "regDttm": "", "updDttm": "" }];
        }
      });
      this.subSetMenuList = pushData;
      this.toast('새로운 세트메뉴에 서브메뉴를 추가합니다!');
      console.log('세트메뉴 선택을 해야지');
    }
    else {
      var flag: boolean = true;
      this.subSetMenuList.forEach(elementSet => {
        //세트 메뉴의 서브를 보면서 이미 있는 애인지 한번 확인
        if (item.menuNo == elementSet.subMenuNo)
          flag = false;
      });

      if (flag && this.selectedSetMenuNo != -1)
        this.setting.subSetMenu.post(this.selectedSetMenuNo, item.menuNo, "", 1)
          .subscribe(
          res => {
            if (res.json().state == "success") {
              this.calUnitPrice(item.saleAmt)
              this.toast('서브메뉴가 성공적으로 추가 되었습니다.');
            }
            else if (res.json().state == "fail")
              this.toast('서브메뉴가 추가되지 않았습니다. 확인해 주세요.');

            console.log(res.json());
            this.getSubMenuList();
          },
          err => {
            if (err == "network")
              this.toast('인터넷 연결을 확인해 주세요.');
            else  
              this.toast('오류입니다. 다시시도해 주세요.');
          }
          );
      else if (flag && this.selectedSetMenuNo == -1) {
        this.menuList.forEach(element => {
          if (item.menuNo == element.menuNo) {
            this.calUnitPrice(item.saleAmt)
            this.subSetMenuList.push({ "menuName": element.menuName, "setMenuNo": this.selectedSetMenuNo, "subMenuNo": item.menuNo, "menuCNT": 1, "menuText": "", "saleAmt": item.saleAmt, "regDttm": "", "updDttm": "" });
          }
        });
      } else {
        this.toast('이미 선택된 메뉴 입니다');
      }
    }
  }
  modifySubMenu(item) {
    this.setting.subSetMenu.put(this.selectedSetMenuNo, item.subMenuNo, item.menuText, item.menuCNT)
      .subscribe(
      res => {
        if (res.json().state == "success")
          this.toast('성공적으로 수정되었습니다!');
        else if (res.json().state == "fail")
          this.toast('오류입니다. 다시 시도해 주세요!')
        console.log(res.json());
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  deleteSubMenu(item, idx) {
    this.calUnitPrice(-item.saleAmt, item.menuCNT);
    if (this.selectedSetMenuNo != -1) {
      this.setting.subSetMenu.delete(this.selectedSetMenuNo, item.subMenuNo)
        .subscribe(
        res => {
          if (res.json().state == "success")
            this.toast('성공적으로 삭제되었습니다!');
          else
            this.toast('오류입니다. 다시 시도해 주세요!');

          console.log(res.json());
          this.getSubMenuList();
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else  
            this.toast('오류입니다. 다시시도해 주세요.');
        }
        );
    } else {
      this.subSetMenuList.splice(idx, 1);
    }
  }

  calSubMenuCNT(item, i) {
    if (i == -1)
      if (item.menuCNT <= 1)
        this.toast("수량 0개는 없습니다. 삭제 버튼을 눌러주세요");
      else {
        this.calUnitPrice(-item.saleAmt)
        item.menuCNT = String(Number(item.menuCNT) - 1);
      }
    else {
      this.calUnitPrice(item.saleAmt)
      item.menuCNT = String(Number(item.menuCNT) + 1);
    }

  }
  calUnitPrice(saleAmt, cnt = 1) {
    for (var i = 0; i < cnt; i++) {
      this.item.unitPrice = Number(this.item.unitPrice) + Number(saleAmt);

    }
  }


  newSetMenu() {
    this.selectedSetMenuNo = -1;
    this.selectedSetItem = -1;
    this.subSetMenuList = null;
    this.item = {
      "menuNo": "", "menuCode": "", "menuName": "", "menuNameEn": "", "taxType": 1, "taxAmt": "", "unitPrice": "",
      "saleAmt": "", "categoryNo": "", "useYN": 1, "stampUseYN": 1, "soldOutYN": 1, "takeoutDCAmt": "",
      "regUserNo": "", "menuType": 2, "startDt": this.nowdate.getFullYear() + '-' + (this.nowdate.getMonth() < 10 ? '0' : '') + (this.nowdate.getMonth() + 1) + '-' + (this.nowdate.getDate() < 10 ? '0' : '') + this.nowdate.getDate(),
      "endDt": this.endDate.getFullYear() + '-' + (this.endDate.getMonth() < 10 ? '0' : '') + (this.endDate.getMonth() + 1) + '-' + (this.endDate.getDate() < 10 ? '0' : '') + this.endDate.getDate(), "dispOrder": "", "menuBgColor": "", "sublist": ""
    };
    this.item.taxType == "1" ? this.taxType = true : this.taxType = false;
    this.item.useYN == "1" ? this.useYN = true : this.useYN = false;
    this.item.stampUseYN == "1" ? this.stampUseYN = true : this.stampUseYN = false;
    this.item.soldOutYN == "1" ? this.soldOutYN = true : this.soldOutYN = false;


  }
  modifySetMenu() {
    this.item.taxType = Number(this.taxType);
    this.item.useYN = Number(this.useYN);
    this.item.stampUseYN = Number(this.stampUseYN);
    this.item.soldOutYN = Number(this.soldOutYN);

    console.log(this.item);
    this.setting.menu.put(this.item)
      .subscribe(
      data => {
        if (data.json().state == "success")
          this.toast('성공적으로 수정되었습니다!');
        else
          this.toast('오류입니다. 다시 시도해 주세요!');
        console.log(data);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  addnewSetMenu() {
    this.item.taxType = Number(this.taxType);
    this.item.useYN = Number(this.useYN);
    this.item.stampUseYN = Number(this.stampUseYN);
    this.item.soldOutYN = Number(this.soldOutYN);
    this.item.taxAmt = (this.taxType == true) ? Math.floor(this.item.saleAmt / this.taxRatio) : 0;

    this.item.sublist = this.subSetMenuList;

    if (this.subSetMenuList == null) {
      this.toast('서브 메뉴를 선택해 주세요');
    } else if (this.item.menuCode == "" || this.item.menuName == "" || this.item.unitPrice == "" || this.item.saleAmt == "" || this.item.categoryNo == "") {
      this.toast('필수 항목을 채워 주세요 ');
    }
    else {
      this.setting.setMenu.post(this.item)
        .subscribe(
        data => {
          if (data.json().state == "success") {
            this.toast('성공적으로 추가되었습니다!');
            this.newSetMenu();
            this.getSetMenuList(-1);
            this.getMenuCategoryList();
            this.getMenuList(-1);
          }
          else
            this.toast('오류입니다. 다시 시도해 주세요!');
          console.log(data);
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else  
            this.toast('오류입니다. 다시시도해 주세요.');
        }
        );

    }

  }
  deleteSetMenu(item) {
    this.setting.setMenu.delete(item.menuNo)
      .subscribe(
      data => {
        if (data.json().state == "success") {
          this.toast('성공적으로 삭제되었습니다!');
          this.newSetMenu();
          this.getSetMenuList(-1);
          this.getMenuCategoryList();
          this.getMenuList(-1);
        }
        else
          this.toast('오류입니다. 다시 시도해 주세요!');
        console.log(data);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else  
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );

  }

  toast(text) {
    
        if(this.toastInstance) {
          return;
        }
      
        this.toastInstance = this.toastCtrl.create({
          message: text,
          duration: 3000,
          dismissOnPageChange: true,
        });
      
        this.toastInstance.onDidDismiss(() => {
          this.toastInstance = null;
        });
      
        this.toastInstance.present();
      }
    
    
}
