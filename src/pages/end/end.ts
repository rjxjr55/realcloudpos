import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Events, ModalController, Toast, IonicPage } from 'ionic-angular';
import { Sales } from '../../services/sales';
import { DatePipe } from '@angular/common';

@IonicPage()
@Component({
  selector: 'page-end',
  templateUrl: 'end.html'
})
export class EndPage {
  today = new Date();
  salesDttm: any;
  salesDt: any;
  endData = {
    balanceCash: 0,
    closeDttm: "",
    openCash: 0,
    salesDttm: "",
    totalCard: 0,
    totalCash: 0,
    totalPoint: 0,
    totalMileage: 0,
    issueMileage: 0,
  };
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sales: Sales, public toastCtrl: ToastController, private events: Events, private modalCtrl: ModalController) {
    this.getOpenTm();
  }

  ionViewDidLoad() {
    this.events.publish('layout:header', '영업마감', true, false, true);
  }

  //가장 최근의 개시 일자 가지고 오기 
  getOpenTm() {
    this.sales.end.getNo()
      .subscribe(
      data => {
        this.salesDttm = data['_body'];
        let datePipe = new DatePipe('en-US');
        this.salesDt = datePipe.transform(this.salesDttm, 'yyyy-MM-dd');
        this.getEndData(this.salesDt);
        console.log(data);
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('마감할 데이터가 없습니다. 개시를 시작해 주세요.');
      });
  }
  //가장 최근의 개시일자에 해당하는 마감 data 들고오기 
  getEndData(salesDttm) {
    let datePipe = new DatePipe('en-US');
    let date = datePipe.transform(salesDttm, 'yyyy-MM-dd');
    this.sales.end.get(date, date, 1)
      .subscribe(
      data => {
        this.endData = data.json().list[0];
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      });
  }
  //마감하기
  end() {
    this.sales.end.post(this.salesDttm)
      .subscribe(
      data => {
        this.toast('마감이 성공적으로 이루어졌습니다.');
        this.events.publish('nav:go', 4);
        setTimeout(() => { this.events.publish('nav:go', 4); }, 1000);

      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else if (err.json().message == "order left")
          this.toast('결제 되지 않은 테이블이 있습니다. 확인해 주세요.');
        else
          this.toast('영업 마감이 제대로 되지 않았습니다. 확인해 주세요');
      });
  }

  goEndDetail() {
    let datePipe = new DatePipe('en-US');
    let date = datePipe.transform(this.today, 'yyyy-MM-dd');
    let modal = this.modalCtrl.create('StatMenuDailyPage', { startDate: this.salesDt, endDate: date }, {
      cssClass: 'mileage-modal'
    });
    modal.present();

  }

  //기능 준비중 
  preparing() {
    let modal = this.modalCtrl.create('MessageAlertPage', { title: '준비중', message: '준비중인 기능입니다.', color: 'red' }, { cssClass: "message-modal" });
    modal.present();
  }


  toast(text) {

    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

}
