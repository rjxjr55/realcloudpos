import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesAmtModifyPage } from './sales-amt-modify';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    SalesAmtModifyPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesAmtModifyPage),
    AlertHeaderComponentModule,
  ],
  exports: [
    SalesAmtModifyPage
  ]
})
export class SalesAmtModifyPageModule {}
