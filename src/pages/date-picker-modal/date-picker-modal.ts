import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-date-picker-modal',
  templateUrl: 'date-picker-modal.html',
})
export class DatePickerModalPage {

  today = new Date();
  showDatePicker: boolean = false;
  isBegin = false;
  constructor(public params: NavParams, public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log(this.params.get('date'));
    this.today = this.params.get('date');
    this.showDatePicker = true;

    if(this.params.get("isBegin")){
      this.isBegin = this.params.get("isBegin");
      console.log(this.isBegin);
    }
  }

  showDatePickerChange(tf) {
    this.viewCtrl.dismiss({date: this.today});
  }

  OnDataSelected(date) {
    this.today = date;
  }
}
