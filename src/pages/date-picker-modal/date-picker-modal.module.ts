import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DatePickerModalPage } from './date-picker-modal';
import { DatepickerModule } from '../../components/datepicker/datepicker.module';

@NgModule({
  declarations: [
    DatePickerModalPage,
  ],
  imports: [
    IonicPageModule.forChild(DatePickerModalPage),
    DatepickerModule
  ],
  exports: [
    DatePickerModalPage
  ]
})
export class DatePickerModalPageModule {}
