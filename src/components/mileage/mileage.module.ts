import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MileageComponent } from './mileage';
import { SaveMileageContainerPageModule } from "../../pages/save-mileage-container/save-mileage-container.module";
import { SaveStampContainerPageModule } from "../../pages/save-stamp-container/save-stamp-container.module";
import { AlertHeaderComponentModule } from "../alert-header/alert-header.module";
import { NumpadModule } from "../../modules/Numpad/numpad.module";
import { CheckboxModule } from "../check-box/check-box.module";
import { CheckMemberPageModule } from "../../pages/check-member/check-member.module";

@NgModule({
  declarations: [
    MileageComponent,
  ],
  imports: [
    IonicModule, SaveMileageContainerPageModule, SaveStampContainerPageModule,
    AlertHeaderComponentModule, NumpadModule, CheckboxModule,
    CheckMemberPageModule
  ],
  exports: [
    MileageComponent
  ]
})
export class MileageComponentModule {}
