import { Component, ViewChild, Output, Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { ModalController, ToastController, Toast } from 'ionic-angular';
import { LoginSession } from "../../services/login.session";
import { Pay } from "../../services/pay";
import { User } from "../../services/user";
import { SaveMileageContainerPage } from "../../pages/save-mileage-container/save-mileage-container";
import { SaveStampContainerPage } from "../../pages/save-stamp-container/save-stamp-container";
import { CheckMemberPage } from "../../pages/check-member/check-member";
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'mileage-component',
  templateUrl: 'mileage.html',
  providers: [SaveMileageContainerPage, SaveStampContainerPage, DecimalPipe] // 얘네 추가해 달라고 에러뜨길래 추가해따
})
export class MileageComponent {
  @ViewChild('phonenum') phoneInput;
  @ViewChild('saveMile') saveMileageInput;
  @ViewChild('usePoint1') usePoint1Input;
  @ViewChild('saveStamp') saveStampInput;
  @ViewChild('usePoint2') usePoint2Input;

  pageTitle: string = "";
  costTitle: string = '';

  mileageOrStamp: boolean = true;// true -> 마일리지 // false -> 스탬프 // 
  saveOrUse: boolean = true; // true -> 적립 // false -> 사용 // 

  // selectedUser: any = null;
  selectedUser = {
    mileage: 0,
    page: '',
    stampCnt: 0,
    tel: '',
    totalPoint: 0,
    tranCnt: 0,
    userName: '',
    userNo: ''
  } // 적립하려는 회원의 정보(임의로 넣어따)

  beforemileage: number = 0;
  aftermileage: number = 0;
  saveRatio: number = 0;
  stampToSave: number = 0;
  @Input() cost: any = 0; // 결제금액
  tel: any = ''; // 검색할 번호

  pointToUse: number = 0;
  mileageToUse: number = 0;
  pointPlusMileage: number = 0;
  pointOrMileage: boolean; // true => 포인트 // false => 마일리지

  which: any = 0; // cash or card
  checkOrNot: Array<boolean> = []; //[0] => 카드 // [1] => 현금

  selectedNumpad: any = 1; // 1 => 기본numpad // 2 => %붙은 numpad
  serviceTitle: string = "교환 상품";

  stampList: Array<any> = [];
  selectedStamp: any = { exchange: '교환상품을 선택하세요.', unit: 0 };
  selectedStampIdx: number = -1;
  showDrop: boolean = false;

  @Output() goPayData = new EventEmitter();

  private toastInstance: Toast;

  constructor(public modalCtrl: ModalController, private loginSession: LoginSession, private pay: Pay, private user: User, public SaveMileageContainerPage: SaveMileageContainerPage, public SaveStampContainerPage: SaveStampContainerPage, private decimalPipe: DecimalPipe, public toastCtrl: ToastController, private ref: ChangeDetectorRef) {
    // console.log('Hello MileageComponent Component');
    this.openMembership();

  }

  ngOnInit() {
    this.costTitle = "결제금액: " + this.decimalPipe.transform(this.cost) + "원"; //헤더에 들어갈 title
    this.phoneInput.focus();
    this.whichNumpad(1);

    if (this.loginSession.getInfo().mileageStampOpt == "1" || this.loginSession.getInfo().mileageStampOpt == 1)
      this.mileageOrStamp = true;
    else
      this.mileageOrStamp = false;
  }

  findMember() {
    let modal = this.modalCtrl.create('CheckMemberPage', { tel: this.tel, mileageOrStamp: this.mileageOrStamp }, { cssClass: 'message-modal' });
    modal.onDidDismiss(data => {
      if (data != null) {
        this.selectedUser = {
          mileage: Number(data.mileage),
          page: '1',
          stampCnt: Number(data.stamp),
          tel: data.tel,
          totalPoint: Number(data.point),
          tranCnt: 0,
          userName: data.userName,
          userNo: data.userNo
        };
        this.initialize(true);

        if (!this.mileageOrStamp) {
          this.user.stampActive(this.selectedUser.userNo).subscribe(
            data => {
              this.stampList = data.json();
            },
            err => {
              this.toast('이 고객이 사용할수 있는 스탬프가 없습니다.');
            }
          );
        }
        else {
          this.cardOrCash(0);
        }
      }
    });
    modal.present();
  }

  initialize(val) {
    if (this.mileageOrStamp) {
      if (val == true) {
        this.saveOrUse = true;
        this.pointToUse = 0;
        this.mileageToUse = 0;
        this.pointPlusMileage = 0;
        this.saveMileageInput.focus();
        this.whichNumpad(2);
        this.cardOrCash(this.checkOrNot[0] ? 0 : 1);
      }
      else if (val == false) {
        this.saveOrUse = false;
        this.saveRatio = 0;
        this.beforemileage = 0;
        this.usePoint1Input.focus();
        this.whichNumpad(1);
      }
    }
    else {
      if (val == true) {
        this.saveOrUse = true;
        this.pointToUse = 0;
        this.saveStampInput.focus();
        this.whichNumpad(2);

      }
      else if (val == false) {
        this.saveOrUse = false;
        this.stampToSave = 0;
        setTimeout(() => {
          this.usePoint2Input.focus();
          this.whichNumpad(1);
        }, 500);
      }
    }


  }

  openMembership() {
    if (this.mileageOrStamp)
      this.pageTitle = '마일리지 적립';
    else this.pageTitle = '스탬프 적립';

    if (this.mileageOrStamp) {
      this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
      this.aftermileage = Number(this.selectedUser.mileage) + this.beforemileage;
    }

  }

  closeMembership(IsUseM = false) {
    if (IsUseM) {
      this.savePayData(true);
    } else {
      this.savePayData(false);
    }
  }

  whichNumpad(val) {
    this.selectedNumpad = val;
    console.log(this.selectedNumpad);

  }

  valueChange(val) {
    if (this.mileageOrStamp && this.saveOrUse == true) {
      this.saveRatio = val;
      this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
      this.aftermileage = Number(this.selectedUser.mileage) + this.beforemileage;
    }
    else if (this.mileageOrStamp && this.saveOrUse == false) {
      this.pointPlusMileage = Number(this.pointToUse) + Number(this.mileageToUse);
    }
    else if (!this.mileageOrStamp && this.saveOrUse == true) {

    }
    else if (!this.mileageOrStamp && this.saveOrUse == false) {
    }
  }

  costChange(val) {
    this.cost = val;
    this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
    this.aftermileage = Number(this.selectedUser.mileage) + this.beforemileage;
  }

  mileagePM(val) {
    if (this.selectedUser.userNo == '') {
      this.toast("회원을 선택해 주세요.");
    }
    else {
      if (val == '+')
        this.saveRatio++;
      else if (val == '-' && this.saveRatio > 0)
        this.saveRatio--;

      this.beforemileage = Math.floor(this.cost * ((this.saveRatio) / 100));
      this.aftermileage = Number(this.selectedUser.mileage) + this.beforemileage;
    }
  }

  stampPM(val) {
    if (this.selectedUser.userNo == '') {
      this.toast("회원을 선택해 주세요.");
    }
    else {
      if (this.saveOrUse) {
        if (val == '+')
          this.stampToSave++;
        else if (val == '-' && this.stampToSave > 0)
          this.stampToSave--;
      }
      else {
        if (val == '+')
          this.selectedStamp.unit++;
        else if (val == '-' && this.selectedStamp.unit > 0)
          this.selectedStamp.unit--;
      }
    }
  }

  cardOrCash(val) {

    if (val == 0) { // 카드적립 선택
      this.checkOrNot[0] = true;
      this.checkOrNot[1] = false;
      if (this.selectedUser.userNo == '') {
        this.toast("회원을 선택해 주세요.");
      }
      this.saveRatio = this.loginSession.getInfo().mileageSaveCard;

    }
    else if (val == 1) { // 현금적립 선택
      this.checkOrNot[0] = false;
      this.checkOrNot[1] = true;
      if (this.selectedUser.userNo == '') {
        this.toast("회원을 선택해 주세요.");
      }
      this.saveRatio = this.loginSession.getInfo().mileageSaveCash;

    }
    this.beforemileage = Math.floor(this.cost * (this.saveRatio) / 100);
    this.aftermileage = Number(this.selectedUser.mileage) + this.beforemileage;


  }

  inputClear(val) { // true => 포인트 // false => 마일리지 //
    if (val == true) this.pointToUse = 0;
    else this.mileageToUse = 0;
  }

  useAll(val) { // true => 포인트 // false => 마일리지 //
    if (this.selectedUser.userNo == '') {
      this.toast("회원을 선택해 주세요.");
      return;
    }
    if (val == true) {
      //포인트를 사용할 시 
      if (this.cost >= this.selectedUser.totalPoint)
        this.pointToUse = this.selectedUser.totalPoint;
      else
        this.pointToUse = this.cost;

    }
    else {
      //마일리지를 사용할 시 
      if (this.cost >= this.selectedUser.mileage)
        this.mileageToUse = this.selectedUser.mileage;
      else
        this.mileageToUse = this.cost;
    }
  }

  saveComplete() {
    let isSaveMileage = false;
    if (this.selectedUser.userNo == '') {
      this.toast("회원을 선택해 주세요.");
      return 0;
    }
    if (this.pointToUse > this.selectedUser.totalPoint) {
      this.toast("가용 포인트를 초과하였습니다. 잔여 포인트를 확인해 주세요.")
      return 0;
    }
    if (this.mileageToUse > this.selectedUser.mileage) {
      this.toast("가용 마일리지를 초과하였습니다. 잔여 마일리지를 확인해 주세요.")
      return 0;
    }
    if (this.selectedStamp.unit > this.selectedUser.stampCnt) {
      this.toast("가용 스탬프를 초과하였습니다. 잔여 스탬프를 확인해 주세요.");
      return 0;
    }
    if (this.pointPlusMileage > this.cost) {
      this.toast("포인트와 마일리지 금액이 결제 금액보다 많습니다. 확인해 주세요");
      return 0;
    }
    if (this.pointToUse && this.selectedStamp.unit) {
      this.toast("포인트와 스탬프를 동시에 사용할 수 없습니다. 확인해 주세요");
      return 0;
    }

    if (this.mileageOrStamp) {
      if (this.saveOrUse)
        this.saveMileage(this.selectedUser);
      else
        isSaveMileage = true;
    } //마일리지
    else {
      if (this.saveOrUse)
        this.saveStamp(this.selectedUser);
      else if (this.pointToUse)
        isSaveMileage = true;
      else {
        if (this.pointToUse == 0 && this.selectedStamp.exchange == '교환상품을 선택하세요.') {
          this.toast('교환상품을 선택하세요.');
          return 0;
        }
        if (this.selectedStamp.unit > this.selectedUser.stampCnt) {
          this.toast('사용자의 스탬프가 부족합니다!');
          return 0;
        }
        this.useStamp(this.selectedUser);
      }
    } //스탬프

    this.closeMembership(isSaveMileage);



  }

  saveMileage(selectedUser) {

    this.pay.pointPay.post(selectedUser.tel, selectedUser.userNo, this.cost, 0, 1, this.beforemileage, this.saveRatio).subscribe(
      data => {
        if (data.json().state == "success") {
          let modal = this.modalCtrl.create('SaveMileageContainerPage', { beforemileage: this.beforemileage, aftermileage: this.aftermileage }, { cssClass: 'message-modal' });
          modal.present();
        }
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('아이디 혹은  비밀번호를 확인해 주세요.');
      }
    );
  }


  saveStamp(selectedUser) {
    console.log('this.stampToSave' + this.stampToSave);
    this.pay.pointPay.post(selectedUser.tel, selectedUser.userNo, this.cost, this.stampToSave, 1, 0, 0).subscribe(
      data => {
        let modal = this.modalCtrl.create('SaveStampContainerPage', { saveOrUse: true, beforeStamp: this.stampToSave, afterStamp: (this.selectedUser.stampCnt + this.stampToSave) }, { cssClass: 'message-modal' });
        modal.present();
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('아이디 혹은  비밀번호를 확인해 주세요.');
      }
    );
  }

  useStamp(selectedUser) {
    // if (!selectedUser.userNo) {
    //   this.toast('비회원은 스탬프를 사용하실수 없습니다.');
    //   return;
    // } else if (this.selectedStamp.exchange == '교환상품을 선택하세요.') {
    //   this.toast('교환상품을 선택하세요.');
    //   return;
    // } else if (this.selectedStamp.unit > selectedUser.stampCnt) {
    //   this.toast('사용자의 스탬프가 부족합니다!');
    //   return;
    // }
    this.user.stampUse(selectedUser.tel, this.selectedStamp.unit, this.selectedStamp.exchange).subscribe(
      res => {
        let modal = this.modalCtrl.create('SaveStampContainerPage', { saveOrUse: false, beforeStamp: this.selectedStamp.unit, afterStamp: (this.selectedUser.stampCnt - this.selectedStamp.unit) }, { cssClass: 'message-modal' });
        modal.present();
      },
      error => {
      }
    );
  }

  savePayData(val = false) {
    //결제 page에 값 넘겨주기    
    let payInfo = {
      barcode: this.selectedUser.tel,
      userNo: this.selectedUser.userNo,
      curPoint: this.selectedUser.totalPoint,
      pointAmt: this.pointToUse,
      mileageAmt: this.mileageToUse,
      saveOrNot: val
    }
    this.goPayData.emit(payInfo);
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

  stampClicked(event) {
    console.log(this.selectedStampIdx);
    if (this.selectedStampIdx != -1) {
      this.selectedStamp.exchange = this.stampList[this.selectedStampIdx].exchange;
      this.selectedStamp.unit = this.stampList[this.selectedStampIdx].unit;
    }
    else {
      this.selectedStamp.exchange = '교환상품을 선택하세요.';
      this.selectedStamp.unit = 0;
    }
    this.ref.detectChanges();
  }

}
