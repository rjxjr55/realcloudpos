import { NgModule, forwardRef } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TablePushComponent } from './table-push';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CheckboxModule } from '../../components/check-box/check-box.module';


@NgModule({
  declarations: [
    TablePushComponent
  ],
  imports: [
    IonicModule,
    CheckboxModule,
  ],
  providers: [],
  exports: [
    TablePushComponent
  ]
})
export class TablePushComponentModule {}