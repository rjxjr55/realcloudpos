import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import * as _ from 'lodash';

/*
  Generated class for the Table component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
    selector: 'pos-table3',
    templateUrl: 'table-push.html'
})
export class TablePushComponent {
    @Input() header;
    @Input() data: any[] = [{ checked: false }];
    @Input() keys;
    @Input() checked:boolean = false;
    @Input() total:number = 0;
    @Input() isMileage:boolean = true;

    @Output() OnPageChanged = new EventEmitter();
    @Output() OnButtonClicked = new EventEmitter();
    @Output() OnSelectOrder = new EventEmitter();

    ascendingOrder: Array<boolean> = [false, false, false, false, false, false, false, false, false, false];

    constructor(private ref: ChangeDetectorRef) { }

    ngOnInit() {
        this.data = [{ checked: false }];
    }

    dataSort(idx) {
        let temp = _.cloneDeep(this.data);

        if (!this.ascendingOrder[idx]) {
            temp.sort((a, b) => {
                if (a[this.keys[idx]] > b[this.keys[idx]])
                    return -1;
                if (a[this.keys[idx]] < b[this.keys[idx]])
                    return 1;
                return 0;
            });
            this.ascendingOrder.fill(false);
            this.ascendingOrder[idx] = true;
        } else {
            temp.sort((a, b) => {
                if (a[this.keys[idx]] > b[this.keys[idx]])
                    return 1;
                if (a[this.keys[idx]] < b[this.keys[idx]])
                    return -1;
                return 0;
            });
            this.ascendingOrder[idx] = false;
        }
        this.data = _.cloneDeep(temp);
        this.ref.detectChanges();

    }

    selectOrder(idx, data) {
        console.log(idx);
        this.data.forEach(element => {
            element['checked'] = false;
        });
        this.data[idx]['checked'] = true;
        this.OnSelectOrder.emit(data);
    }

    doInfinite() {
        this.OnPageChanged.emit();
    }
}
