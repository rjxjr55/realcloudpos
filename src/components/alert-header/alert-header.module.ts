import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AlertHeaderComponent } from './alert-header';

@NgModule({
  declarations: [
    AlertHeaderComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    AlertHeaderComponent
  ]
})
export class AlertHeaderComponentModule {}
