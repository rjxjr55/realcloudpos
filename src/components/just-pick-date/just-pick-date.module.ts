import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { JustPickDateComponent } from './just-pick-date';
import { DatepickerModule } from '../datepicker/datepicker.module';

@NgModule({
  declarations: [
    JustPickDateComponent,
  ],
  imports: [
    IonicModule,
    DatepickerModule
  ],
  exports: [
    JustPickDateComponent
  ]
})
export class JustPickDateComponentModule {}
