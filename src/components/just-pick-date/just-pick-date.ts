import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'just-pick-date',
  templateUrl: 'just-pick-date.html'
})
export class JustPickDateComponent {
  @Output() dateChanged = new EventEmitter<Date>();
  @Input() plusNminus : boolean = true;
  @Input() date: Date = new Date();
  
  constructor() {
  }

  minus() {
    var nDate = new Date();
    nDate.setTime(this.date.getTime() + -1 * 86400000);
    this.date = nDate;
    this.dateChanged.emit(this.date);
  }

  plus() {
    var nDate = new Date();
    nDate.setTime(this.date.getTime() + 1 * 86400000);
    this.date = nDate;
    this.dateChanged.emit(this.date);
  }

  setDate(date) {
    this.date = date;
  }
}
