import { Component, Input, Output, EventEmitter } from '@angular/core';
// import * as _ from 'lodash';

/*
  Generated class for the Table component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
    selector: 'pos-table2',
    templateUrl: 'table-setting.html'
})
export class TableSettingComponent {
    @Input() header;
    @Input() data: any[] = [{}];
    @Input() keys;
    @Input() listDetail;
    @Input() total = 0;
    @Output() OnPageChanged = new EventEmitter();

    selectedIndex = -1;
    constructor() { }
    

    ngOnChanges() {
        this.selectedIndex = -1;
    }

    read(index) {
        if (this.selectedIndex == index)
            this.selectedIndex = -1;
        else {
            this.selectedIndex = index;
        }
    }

    nl2br(text) {
        return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }

    doInfinite() {
        this.OnPageChanged.emit();
    }
}
