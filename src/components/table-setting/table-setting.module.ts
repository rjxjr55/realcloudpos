import { NgModule, forwardRef } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TableSettingComponent } from './table-setting';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CheckboxModule } from '../../components/check-box/check-box.module';


@NgModule({
  declarations: [
    TableSettingComponent
  ],
  imports: [
    IonicModule,
    CheckboxModule,
  ],
  providers: [],
  exports: [
    TableSettingComponent
  ]
})
export class TableSettingComponentModule {}