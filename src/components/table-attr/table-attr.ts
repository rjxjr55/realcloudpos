import { Component, Input, Output, EventEmitter } from '@angular/core';
// import * as _ from 'lodash';

/*
  Generated class for the Table component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
    selector: 'pos-table4',
    templateUrl: 'table-attr.html'
})
export class TableAttrComponent {
    @Input() header;
    @Input() data: any[] = [{}];
    @Input() keys;
    @Input() listDetail;
    @Output() OnButtonClicked = new EventEmitter();

    constructor() { }

    toggle(data) {
        this.OnButtonClicked.emit(data);
    }
}
