import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'check-box',
  templateUrl: 'check-box.html',
  styles: [
    ` 
      :host { 
        width: 25px; 
        height: 25px; 
      
        @media screen and (max-width: 1366px) {
          width: 20px;
          height: 20px;
        }
      } 
    `
  ]
})
export class CheckBoxComponent{
  _checked: boolean = false;
  _disabled: boolean = false;

  CheckColors: { [id: string]: string };
  UnCheckColors: { [id: string]: string };
  @Input() get checked() { return this._checked; }
  @Input() checkColor;
  @Input() unCheckColor;
  
  set checked(val: boolean) {
    this._checked = val;
    this.checkedChange.emit(val);
    this.CheckColors = {
      'white': '#FFFFFF',
      'blue': '#1C466B',
      'red': '#F05161',
      'green' : '#00BEBB'
    };
    this.UnCheckColors = {
      'lightgray': '#E7EBEF',
      'deepgray': '#BBBEBF',
      'red': '#FFE3E3',
      'green' : '#D0F2F1'
    };

  }
  @Output() checkedChange = new EventEmitter();

  @Input() get disabled() { return this._disabled; }

  constructor() {
  }

  getCheckedColor(): string {
    let color;
    if (this.checkColor) {
      color = this.CheckColors[this.checkColor];
      
    } else{
      color = this.CheckColors['white'];
    }
    return color;
  }

  getUnCheckedColor(): string {
    let color;
    if (this.unCheckColor) {
      color = this.UnCheckColors[this.unCheckColor];
      
    } else{
      color = this.UnCheckColors['lightgray'];
    }
    return color;
  }

}