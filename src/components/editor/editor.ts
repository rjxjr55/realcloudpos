import { Component, Input, Output, EventEmitter } from '@angular/core';
// import * as _ from 'lodash';
import { LoadingController } from 'ionic-angular';
import { Headers, RequestOptions, Http } from "@angular/http";

declare const Quill: any;

/*
  Generated class for the Table component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
    selector: 'pos-editor',
    templateUrl: 'editor.html'
})
export class EditorComponent {
    contents: any;
    quill: any;

    @Input() location: string;//push or ??
    @Input() buttonClicked: boolean = false;
    @Input() clear: boolean = false;
    @Input() text: any = "";
    @Output() OnButtonClicked = new EventEmitter();
    // @Input() locatiosssn = new EventEmitter();

    constructor(public loadingCtrl: LoadingController, public http: Http) { }

    ngAfterContentInit() {
        setTimeout(() => {
            this.quill = new Quill('#editor', {
                theme: 'snow',
                modules: {
                    toolbar: {
                        container: [
                            [{ header: [1, 2, false] }],
                            ['bold', 'italic', 'underline'],
                            ['image']
                        ],
                        handlers: {
                            image: () => {
                                let fileInput = this.quill.container.querySelector('input.ql-image[type=file]');
                                if (fileInput == null) {
                                    fileInput = document.createElement('input');
                                    fileInput.setAttribute('type', 'file');
                                    fileInput.setAttribute('accept', 'image/*');
                                    fileInput.classList.add('ql-image');
                                    fileInput.classList.add('hidden');
                                    fileInput.addEventListener('change', () => {
                                        if (fileInput.files != null && fileInput.files[0] != null) {
                                            let range = this.quill.getSelection(true);
                                            let index = range.index + range.length;

                                            this.uploadImage(fileInput.files[0], (url) => {
                                                this.quill.insertEmbed(index, 'image', url);
                                            });
                                        }
                                    });
                                    this.quill.container.appendChild(fileInput);
                                }
                                fileInput.click();
                            }
                        }
                    }
                }
            });
            if (this.text != "")
                this.quill.root.innerHTML = this.text;
        }, 0)
        console.log('ngOnInit');
    }

    ngOnChanges() {
        if (this.buttonClicked) {
            this.contents = this.quill.root.innerHTML;
            this.OnButtonClicked.emit(this.contents);
        }
        if (this.clear)
            this.quill.root.innerHTML = '<p><br></p>';
    }

    public uploadImage(file, callback) {
        let loading = this.loadingCtrl.create({
            content: '잠시만 기다려 주세요 '
        });
        var fileExt;
        if (file.type == "image/bmp")
            fileExt = ".bmp";
        else if (file.type == "image/png")
            fileExt = ".png";
        else
            fileExt = ".jpg";
        var filename = '' + new Date().getTime() + (Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000) + fileExt;
        var url = "https://s3.ap-northeast-2.amazonaws.com/hgus3/origin/" + this.location + "/" + filename;
        var headers = new Headers({ 'Content-Type': file.type, 'x-amz-acl': 'public-read-write' });
        var options = new RequestOptions({ headers: headers });
        this.http.put(url, file, options).subscribe(res => {
            console.log(res);
        });
        loading.present();

        setTimeout(() => {
            loading.dismiss();
            callback(url);
            //아마존 주소 콜백
        }, 2000);
    }

}
