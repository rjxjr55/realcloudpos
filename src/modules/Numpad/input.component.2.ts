import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ConnectorService } from "./connector.service";

@Component({
  selector: 'numpad-input2',
  templateUrl: 'input.2.html',
  styles: [`
    div {
        background: #dedede;
        display: inline-block;
        height: 20px;
        padding: 0 5px;
    }

    div.focused {
        background: #ababab;
    }
  `]
}) //마일리지 적립 시 %붙여주는 용도로 input2를 만듦


export class InputComponent2 {
    val: number = 0;
    @Input() get value(): number {
         return this.val;   
    };
    set value(val: number) {        
        this.val = val;
        this.valueChange.emit(this.val);
    }
    @Output() valueChange = new EventEmitter();

    constructor(public connector: ConnectorService) {

    }

    focus() {
        this.connector.SetInput(this);
    }
}