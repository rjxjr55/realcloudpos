import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ConnectorService } from "./connector.service";

@Component({
  selector: 'numpad-input',
  templateUrl: 'input.html',
  styles: [`
    div {
        background: #fafafa;
        display: inline-block;
        height: 20px;
        padding: 0 5px;
    }

    div.focused {
        background: #ababab;
    }
  `]
})
export class InputComponent {
    val: number = 0;
    @Input() get value(): number {
         return this.val;   
    };
    set value(val: number) {        
        this.val = val;
        this.valueChange.emit(this.val);
    }
    @Output() valueChange = new EventEmitter();

    constructor(public connector: ConnectorService) {

    }

    focus() {
        this.connector.SetInput(this);
    }
}