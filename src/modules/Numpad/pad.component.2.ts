import { Component } from "@angular/core";
import { ConnectorService } from "./connector.service";
import { InputComponent } from "./input.component";
import { InputComponent2 } from "./input.component.2";

@Component({
  selector: 'numpad2',
  templateUrl: 'pad.html',
})
export class NumpadComponent2 {
    constructor(public connector: ConnectorService) {

    }

    fire(num) {
        if (this.connector.focused.value !== undefined) {
            var value: string = this.connector.focused.value + '';
            if (value.length < 3) {
                if (this.connector.focused instanceof InputComponent2 && value == "0")
                    value = '';
                this.connector.focused.value = this.connector.focused instanceof InputComponent2?parseInt(value + num):(value + num);
            }
        }
    }

    fireClear() {
        if (this.connector.focused.value !== undefined) {
            var value: string = (this.connector.focused.value+'').slice(0, -1);;
            if (this.connector.focused instanceof InputComponent2 && value == '')
                value = '0';
            this.connector.focused.value = this.connector.focused instanceof InputComponent2?parseInt(value):(value);
        }
    }

    fireClearAll() {
        if (this.connector.focused.value !== undefined)
            this.connector.focused.value = this.connector.focused instanceof InputComponent2?0:'';
    }
}