import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ConnectorService } from "./connector.service";

@Component({
    selector: 'string-input',
    templateUrl: 'string-input.html',
    styles: [`
    div {
        background: #dedede;
        display: inline-block;
        height: 20px;
        padding: 0 5px;
    }

    div.focused {
        background: #ababab;
    }
  `]
})
export class StringInputComponent {
    val: string = '';
    
    @Input() placeholder = "휴대폰번호";

    @Input() get value(): string {
        return this.val;
    };

    set value(val: string) {
        this.val = val;
        this.valueChange.emit(this.val);
    }
    @Output() valueChange = new EventEmitter();

    constructor(public connector: ConnectorService) {

    }

    focus() {
        this.connector.SetInput(this);
    }
}