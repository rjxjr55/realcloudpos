import { NgModule }      from '@angular/core';
import { ConnectorService } from "./connector.service";
import { NumpadComponent } from "./pad.component";
import { InputComponent } from "./input.component";
import { CommonModule } from "@angular/common";
import { IonicModule } from "ionic-angular";
import { StringInputComponent } from "./string-input.componen";
import { InputComponent2 } from "./input.component.2";
import { NumpadComponent2 } from "./pad.component.2";
import { SquareModule } from '../../directives/square/sqare.module';

@NgModule({
  declarations: [ NumpadComponent, InputComponent, StringInputComponent, InputComponent2, NumpadComponent2],
  providers: [ ConnectorService ],
  imports: [ CommonModule, IonicModule,SquareModule ],
  exports: [ NumpadComponent, InputComponent, StringInputComponent, InputComponent2, NumpadComponent2]
})
export class NumpadModule { }