// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CATIP } from '../../config';
import { Observable } from 'rxjs/Observable';
// import { Buffer } from 'buffer';

/*
  Generated class for the CatProtocolProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

declare const SMTCAT;

@Injectable()
export class CatProtocolProvider {
  ip;
  socket = null;
  connected = false;

  aliveObs = Observable.interval(3000);
  aliveSub;

  constructor() {
      
  }

  connect(ip = this.ip){
    if (!ip) {
      ip = CATIP;
    }

    this.ip = ip;

    return new Promise((resolve, reject)=>{
      if (this.connected)
        SMTCAT.disconnect(()=>{
          resolve();
        }, ()=>{
          resolve();
        });
      else
        resolve();
    })
    .then(_=>{
      localStorage.setItem('CATIP', ip);
      SMTCAT.connect(ip, 5555, _=>{
        if (_ == 'Connected')
          this.connected = true;
      }, _=>{
        console.log(_);
      });
    });
  }

  disconnect() {
    SMTCAT.disconnect(_=>{
      if (_ == 'Disconnected')
        this.connected = false;
    }, _=>{
      console.log(_);
    });
  }

  // type- 0: 신용, 1: 현금, 2: 은련
  // amt- 금액
  // instalment- 할부개월
  /* arr
  [
    {
      name: '상품명',
      amt: '가격',
      cnt: '수량'
    },
    ...
  ]
  */
  requestPay(type, amt, instalment, arr) {
    return new Promise((resolve, reject)=>{
      SMTCAT.trade(type, amt, instalment, arr, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  print(data) {
    return new Promise((resolve, reject)=>{
      SMTCAT.print(data, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  cancel() {
    return new Promise((resolve, reject)=>{
      SMTCAT.cancel(_=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }
}
