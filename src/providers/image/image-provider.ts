import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpService } from "../../services/http.service";

/*
  Generated class for the ImageProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ImageProvider {

  private resource: string = '/store/image';
  public s3: S3;

  constructor(private httpService: HttpService, public http: Http) {
    this.s3 = new S3(http);
    console.log('Hello ImageProvider Provider');
  }


  get() {
      return this.httpService.get(this.resource);
  } //ok

  post(url, thumbnailS, thumbnailM, thumbnailL, displayOrder) {
      return this.httpService.post(this.resource, {
          'url': url,
          'thumbnailS': thumbnailS,
          'thumbnailM': thumbnailM,
          'thumbnailL': thumbnailL,
          'dispOrder': displayOrder
      });
  }

  delete(displayOrder) {
      return this.httpService.delete(this.resource, {
          'dispOrder': displayOrder
      });
    }
  
  deleteAll() {
      return this.httpService.delete("/store/image/all", {});
    }

}

export class S3 { //ok
    constructor(private http: Http) { }

    put(filename: string, file: any) {
        var headers = new Headers({ 'Content-Type': 'image/jpg', 'x-amz-acl': 'public-read-write' });
        var options = new RequestOptions({ headers:headers });
        var buf = this.blob(file);
        return this.http.put("https://s3.ap-northeast-2.amazonaws.com/hgus3/origin/store/" + filename, buf, options);
    }

    private blob(base64) {
        var u = base64.split(',')[1],
            binary = window.atob(u),
            array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        var typedArray = new Uint8Array(array);
        return typedArray.buffer;

    }
}
