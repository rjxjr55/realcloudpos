// export const HOST = `http://homiego.hssa.me:3003`;//qa 서버
// export const HOST = `http://192.168.0.47:3000`;//엠제이qa
export const HOST = `https://openapi.handong.edu:9444`;//실서버
export const API = `${HOST}/pcp`;
export const GRAPHQL_ENDPOINT = `${HOST}/graphql`;
export const PACKAGE_NAME = `kr.go.pohang.sarang.pos`;
export const CATIP = `192.168.10.10`;
export const IS_GENIE = false;